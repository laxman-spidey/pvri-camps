export class MockModels {
    
     camps :any[] = [{"Camp":"A. M. Vidyalayam.","Camp held on":"2018-03-26","Location":"Vizianagaram","State":"Andhra Pradesh","District":"VIZIANAGARAM ","Mandal":"VIZIANAGARAM ","Camp_Coordinator":"Suresh","Opthalmologist":"Surendher","screenings":1},{"Camp":"Bolakpur Govt High School","Camp held on":"2018-03-26","Location":"Hyderabad","State":"Telangana","District":"HYDERABAD","Mandal":"MUSHEERABAD","Camp_Coordinator":"Lakshmi Narayana","Opthalmologist":"Faheeda","screenings":4}];

     patients: any[]  =   
        [{patient_id: 50,"patient_name": "Eshwari bhai","camp_id": 120,"REFRACTION_LEFT_EYE_ADD_LEFT_EYE": "2.25","UNAIDED_VA_LE": "6_by_24","REFRACTION_LEFT_EYE_CYL": "-18.5","OPTHALMIC_ASSISTANT": "Faheeda","WHITE_RATION_CARD": "No","SCREENING_CARD_PLACE": "Bolakpur Govt. High School","SCREENING_CARD_START_TIME": "1970-01-01 05:24:00","SCREENING_CARD_SERIAL_NO": 245,"UNAIDED_VA_RE": "6_by_12","SCREENING_CARD_HEAD_OF_FAMILY_NAME": "Badram goud","REFRACTION_RIGHT_EYE_ADD_RIGHT_EYE": "2","REFRACTION_RIGHT_EYE_ADD_RIGHT_EYE_VA": "N8","PINHOLE_LE": "6_by_18","SCREENING_CARD_MANDAL": "MUSHEERABAD","SCREENING_CARD_PATIENT_ID": 245,"SCREENING_CARD_DATE_OF_BIRTH": "1993-03-10 00:00:00","SCREENING_CARD_GENDER": "female","FINAL_REMARKS": null,"CHIEF_COMPLAINTS_DURATION": null,"REFRACTION_LEFT_EYE_SPH": "-18.75","A_S_OTHER_CONDITION": null,"SCREENING_CARD_PATIENT_GUARDIAN": "daughter","META_INSTANCE_ID": "uuid:c087194e-a0bd-4494-99d3-c9d77f81c288","REFRACTION_RIGHT_EYE_CYL": "-19","CHIEF_COMPLAINTS_SPH": null,"PINHOLE_RE": null,"SCREENING_CARD_AADHAR": null,"A_S_REFERRAL_ADVICE": null,"REFRACTION_LEFT_EYE_AXIS": null,"REFRACTION_LEFT_EYE_VA": null,"REFRACTION_RIGHT_EYE_SPH": null,"SCREENING_CARD_DISTRICT": null,"SCREENING_CARD_CONTACT_NUMBER": null,"REFRACTION_LEFT_EYE_ADD_LEFT_EYE_VA": null,"CHIEF_COMPLAINTS_OCULAR_SEQUELAE": null,"REFRACTION_RIGHT_EYE_VA": null,"SCREENING_CARD_AGE": null,"A_S_EXAMINATION": null,"REFRACTION_RIGHT_EYE_AXIS": null,"CHIEF_COMPLAINTS_SYSTEMIC_ILLNESS": "None","data_entry_time": 3}, {"patient_id": 51,"patient_name": "Dharmesh goud","camp_id": 120,"REFRACTION_LEFT_EYE_ADD_LEFT_EYE": "2.25","UNAIDED_VA_LE": "6_by_60","REFRACTION_LEFT_EYE_CYL": "-19","OPTHALMIC_ASSISTANT": "Faheeda","WHITE_RATION_CARD": "No","SCREENING_CARD_PLACE": "Bolakpur Govt. High School","SCREENING_CARD_START_TIME": "1970-01-01 05:30:00","SCREENING_CARD_SERIAL_NO": 169,"UNAIDED_VA_RE": "CF-5","SCREENING_CARD_HEAD_OF_FAMILY_NAME": "Veeresh goud","REFRACTION_RIGHT_EYE_ADD_RIGHT_EYE": "2","REFRACTION_RIGHT_EYE_ADD_RIGHT_EYE_VA": "N12","PINHOLE_LE": "6_by_24","SCREENING_CARD_MANDAL": "MUSHEERABAD","SCREENING_CARD_PATIENT_ID": 169,"SCREENING_CARD_DATE_OF_BIRTH": null,"SCREENING_CARD_GENDER": "male","FINAL_REMARKS": null,"CHIEF_COMPLAINTS_DURATION": null,"REFRACTION_LEFT_EYE_SPH": "-19","A_S_OTHER_CONDITION": null,"SCREENING_CARD_PATIENT_GUARDIAN": "son","META_INSTANCE_ID": "uuid:e7c4c8fd-904d-46e9-ba37-7af527cea44c","REFRACTION_RIGHT_EYE_CYL": "-18","CHIEF_COMPLAINTS_SPH": null,"PINHOLE_RE": null,"SCREENING_CARD_AADHAR": null,"A_S_REFERRAL_ADVICE": null,"REFRACTION_LEFT_EYE_AXIS": null,"REFRACTION_LEFT_EYE_VA": null,"REFRACTION_RIGHT_EYE_SPH": null,"SCREENING_CARD_DISTRICT": null,"SCREENING_CARD_CONTACT_NUMBER": null,"REFRACTION_LEFT_EYE_ADD_LEFT_EYE_VA": null,"CHIEF_COMPLAINTS_OCULAR_SEQUELAE": null,"REFRACTION_RIGHT_EYE_VA": null,"SCREENING_CARD_AGE": null,"A_S_EXAMINATION": null,"REFRACTION_RIGHT_EYE_AXIS": null,"CHIEF_COMPLAINTS_SYSTEMIC_ILLNESS": "None","data_entry_time": 3}, {"patient_id": 53,"patient_name": "Sunitha goud","camp_id": 120,"REFRACTION_LEFT_EYE_ADD_LEFT_EYE": "2","UNAIDED_VA_LE": "CF-3","REFRACTION_LEFT_EYE_CYL": "-17.5","OPTHALMIC_ASSISTANT": "Faheeda","WHITE_RATION_CARD": "Yes","SCREENING_CARD_PLACE": "Bolakpur Govt. High School","SCREENING_CARD_START_TIME": "1970-01-01 23:04:00","SCREENING_CARD_SERIAL_NO": 521,"UNAIDED_VA_RE": "6_by_18","SCREENING_CARD_HEAD_OF_FAMILY_NAME": "Muthiraj Goud","REFRACTION_RIGHT_EYE_ADD_RIGHT_EYE": "3","REFRACTION_RIGHT_EYE_ADD_RIGHT_EYE_VA": "N12","PINHOLE_LE": "Hm+","SCREENING_CARD_MANDAL": "MUSHEERABAD","SCREENING_CARD_PATIENT_ID": 521,"SCREENING_CARD_DATE_OF_BIRTH": "1992-01-15 00:00:00","SCREENING_CARD_GENDER": "female","FINAL_REMARKS": null,"CHIEF_COMPLAINTS_DURATION": null,"REFRACTION_LEFT_EYE_SPH": "-17","A_S_OTHER_CONDITION": null,"SCREENING_CARD_PATIENT_GUARDIAN": "daughter","META_INSTANCE_ID": "uuid:74c8de4b-640f-4ab3-b2cf-7dddf3393aed","REFRACTION_RIGHT_EYE_CYL": "-19.25","CHIEF_COMPLAINTS_SPH": null,"PINHOLE_RE": null,"SCREENING_CARD_AADHAR": null,"A_S_REFERRAL_ADVICE": null,"REFRACTION_LEFT_EYE_AXIS": null,"REFRACTION_LEFT_EYE_VA": null,"REFRACTION_RIGHT_EYE_SPH": null,"SCREENING_CARD_DISTRICT": null,"SCREENING_CARD_CONTACT_NUMBER": null,"REFRACTION_LEFT_EYE_ADD_LEFT_EYE_VA": null,"CHIEF_COMPLAINTS_OCULAR_SEQUELAE": null,"REFRACTION_RIGHT_EYE_VA": null,"SCREENING_CARD_AGE": null,"A_S_EXAMINATION": null,"REFRACTION_RIGHT_EYE_AXIS": null,"CHIEF_COMPLAINTS_SYSTEMIC_ILLNESS": "None","data_entry_time": 3}, {"patient_id": 54,"patient_name": "Saritha","camp_id": 120,"REFRACTION_LEFT_EYE_ADD_LEFT_EYE": "2","UNAIDED_VA_LE": "CF-2","REFRACTION_LEFT_EYE_CYL": "-18.75","OPTHALMIC_ASSISTANT": "Faheeda","WHITE_RATION_CARD": "Yes","SCREENING_CARD_PLACE": "Bolakpur Govt. High School","SCREENING_CARD_START_TIME": "1970-01-01 23:08:00","SCREENING_CARD_SERIAL_NO": 543,"UNAIDED_VA_RE": "Hm+","SCREENING_CARD_HEAD_OF_FAMILY_NAME": "Narsanna reddy","REFRACTION_RIGHT_EYE_ADD_RIGHT_EYE": "2.25","REFRACTION_RIGHT_EYE_ADD_RIGHT_EYE_VA": "N8","PINHOLE_LE": "6_by_60","SCREENING_CARD_MANDAL": "MUSHEERABAD","SCREENING_CARD_PATIENT_ID": 543,"SCREENING_CARD_DATE_OF_BIRTH": "1977-07-27 00:00:00","SCREENING_CARD_GENDER": "female","FINAL_REMARKS": null,"CHIEF_COMPLAINTS_DURATION": null,"REFRACTION_LEFT_EYE_SPH": "-17.75","A_S_OTHER_CONDITION": null,"SCREENING_CARD_PATIENT_GUARDIAN": "daughter","META_INSTANCE_ID": "uuid:13319c58-4146-46bd-a2f0-8e659100c1f2","REFRACTION_RIGHT_EYE_CYL": "-17.25","CHIEF_COMPLAINTS_SPH": null,"PINHOLE_RE": null,"SCREENING_CARD_AADHAR": null,"A_S_REFERRAL_ADVICE": null,"REFRACTION_LEFT_EYE_AXIS": null,"REFRACTION_LEFT_EYE_VA": null,"REFRACTION_RIGHT_EYE_SPH": null,"SCREENING_CARD_DISTRICT": null,"SCREENING_CARD_CONTACT_NUMBER": null,"REFRACTION_LEFT_EYE_ADD_LEFT_EYE_VA": null,"CHIEF_COMPLAINTS_OCULAR_SEQUELAE": null,"REFRACTION_RIGHT_EYE_VA": null,"SCREENING_CARD_AGE": null,"A_S_EXAMINATION": null,"REFRACTION_RIGHT_EYE_AXIS": null,"CHIEF_COMPLAINTS_SYSTEMIC_ILLNESS": "None","data_entry_time": 3}, {"patient_id": 56,"patient_name": "Sreedhar prasad","camp_id": 121,"REFRACTION_LEFT_EYE_ADD_LEFT_EYE": "2.25","UNAIDED_VA_LE": "CF-4","REFRACTION_LEFT_EYE_CYL": "-17.25","OPTHALMIC_ASSISTANT": "Surendher","WHITE_RATION_CARD": "Yes","SCREENING_CARD_PLACE": "A. M. Vidyalayam","SCREENING_CARD_START_TIME": "1970-01-01 01:58:00","SCREENING_CARD_SERIAL_NO": 163,"UNAIDED_VA_RE": "CF-2","SCREENING_CARD_HEAD_OF_FAMILY_NAME": "Durga prasad","REFRACTION_RIGHT_EYE_ADD_RIGHT_EYE": "2","REFRACTION_RIGHT_EYE_ADD_RIGHT_EYE_VA": "N8","PINHOLE_LE": "6_by_60","SCREENING_CARD_MANDAL": "VIZIANAGARAM","SCREENING_CARD_PATIENT_ID": 163,"SCREENING_CARD_DATE_OF_BIRTH": "1964-04-09 00:00:00","SCREENING_CARD_GENDER": "male","FINAL_REMARKS": null,"CHIEF_COMPLAINTS_DURATION": null,"REFRACTION_LEFT_EYE_SPH": "-17.25","A_S_OTHER_CONDITION": null,"SCREENING_CARD_PATIENT_GUARDIAN": "son","META_INSTANCE_ID": "uuid:2bbff82d-9f25-45c2-a1e3-e31c07466955","REFRACTION_RIGHT_EYE_CYL": "-18.25","CHIEF_COMPLAINTS_SPH": null,"PINHOLE_RE": null,"SCREENING_CARD_AADHAR": null,"A_S_REFERRAL_ADVICE": null,"REFRACTION_LEFT_EYE_AXIS": null,"REFRACTION_LEFT_EYE_VA": null,"REFRACTION_RIGHT_EYE_SPH": null,"SCREENING_CARD_DISTRICT": null,"SCREENING_CARD_CONTACT_NUMBER": null,"REFRACTION_LEFT_EYE_ADD_LEFT_EYE_VA": null,"CHIEF_COMPLAINTS_OCULAR_SEQUELAE": null,"REFRACTION_RIGHT_EYE_VA": null,"SCREENING_CARD_AGE": null,"A_S_EXAMINATION": null,"REFRACTION_RIGHT_EYE_AXIS": null,"CHIEF_COMPLAINTS_SYSTEMIC_ILLNESS": "None","data_entry_time": 3}]
    ;

    
    campsMonthwise: any = {
            chartType: 'line',
            datasets : {
                
                2017: [
                    {
                        label: 'Sales',
                        data : [2.2, 2.9, 3.9, 2.5, 3.8, 3.2, 2.9, 1.9, 3, 3.4, 4.1, 3.8],
                        fill : 'start'

                    }
                ],
                2018: [
                    {
                        label: 'Sales',
                        data : [3.9, 2.5, 3.8, 4.1, 1.9, 3, 3.8, 3.2, 2.9, 3.4, 2.2, 2.9],
                        fill : 'start'

                    }
                ]

            },
            labels   : ['JAN', 'FEB', 'MAR', 'APR', 'MAY', 'JUN', 'JUL', 'AUG', 'SEP', 'OCT', 'NOV', 'DEC'],
            colors   : [
                {
                    borderColor              : '#42a5f5',
                    backgroundColor          : '#42a5f5',
                    pointBackgroundColor     : '#1e88e5',
                    pointHoverBackgroundColor: '#1e88e5',
                    pointBorderColor         : '#ffffff',
                    pointHoverBorderColor    : '#ffffff'
                }
            ],
            options  : {
                spanGaps           : false,
                legend             : {
                    display: false
                },
                maintainAspectRatio: false,
                layout             : {
                    padding: {
                        top  : 32,
                        left : 32,
                        right: 32
                    }
                },
                elements           : {
                    point: {
                        radius          : 4,
                        borderWidth     : 2,
                        hoverRadius     : 4,
                        hoverBorderWidth: 2
                    },
                    line : {
                        tension: 0
                    }
                },
                scales             : {
                    xAxes: [
                        {
                            gridLines: {
                                display       : false,
                                drawBorder    : false,
                                tickMarkLength: 18
                            },
                            ticks    : {
                                fontColor: '#ffffff'
                            }
                        }
                    ],
                    yAxes: [
                        {
                            display: false,
                            ticks  : {
                            //     min     : 1.5,
                            //     max     : 1000,
                            //     stepSize: 0.5
                            }
                        }
                    ]
                },
                plugins            : {
                    filler      : {
                        propagate: false
                    },
                    xLabelsOnTop: {
                        active: true
                    }
                }
            }
        };
    
    campsConductedChart = {
            camps: {
                value   : 0,
                ofTarget: 0
            },
            chartType : 'bar',
            datasets: [
                {
                    label: 'Camps',
                    data: [0, 0, 0, 0, 0, 0]
                }
            ],
            labels: ['Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'],
            colors    : [
                {
                    borderColor    : '#42a5f5',
                    backgroundColor: '#42a5f5'
                }
            ],
            options   : {
                spanGaps           : false,
                legend             : {
                    display: false
                },
                maintainAspectRatio: false,
                layout             : {
                    padding: {
                        top   : 24,
                        left  : 16,
                        right : 16,
                        bottom: 16
                    }
                },
                scales             : {
                    xAxes: [
                        {
                            display: false
                        }
                    ],
                    yAxes: [
                        {
                            display: false,
                            // ticks  : {
                            //     min: 100,
                            //     max: 500
                            // }
                        }
                    ]
                }
            }
        };
        
        
        screeningsConductedChart = {
            screenings: {
                value   : '0',
                ofTarget: 0
            },
            chartType  : 'line',
            datasets   : [
                {
                    label: 'Screenings',
                    data : [0, 0, 0, 0, 0, 0, 0],
                    fill : false
                }
            ],
            labels     : ['Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'],
            colors     : [
                {
                    borderColor: '#5c84f1'
                }
            ],
            options    : {
                spanGaps           : false,
                legend             : {
                    display: false
                },
                maintainAspectRatio: false,
                elements           : {
                    point: {
                        radius          : 2,
                        borderWidth     : 1,
                        hoverRadius     : 2,
                        hoverBorderWidth: 1
                    },
                    line : {
                        tension: 0
                    }
                },
                layout             : {
                    padding: {
                        top   : 24,
                        left  : 16,
                        right : 16,
                        bottom: 16
                    }
                },
                scales             : {
                    xAxes: [
                        {
                            display: false
                        }
                    ],
                    yAxes: [
                        {
                            display: false,
                            ticks  : {
                                 // min: 100,
                                 // max: 500
                            }
                        }
                    ]
                }
            }
        };
        
        surgeriesConductedChart = {
            surgeries   : {
                value   : 882,
                ofTarget: -9
            },
            chartType: 'bar',
            datasets : [
                {
                    label: 'Surgeries',
                    data : [0, 0, 0, 0, 0, 0, 0]
                }
            ],
            labels   : ['Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday', 'Sunday'],
            colors   : [
                {
                    borderColor    : '#f44336',
                    backgroundColor: '#f44336'
                }
            ],
            options  : {
                spanGaps           : false,
                legend             : {
                    display: false
                },
                maintainAspectRatio: false,
                layout             : {
                    padding: {
                        top   : 24,
                        left  : 16,
                        right : 16,
                        bottom: 16
                    }
                },
                scales             : {
                    xAxes: [
                        {
                            display: false
                        }
                    ],
                    yAxes: [
                        {
                            display: false,
                            // ticks  : {
                            //     min: 150,
                            //     max: 500
                            // }
                        }
                    ]
                }
            }
        };
        
        HourlyScreeningsSurgeries = {
            chartType: 'line',
            
            datasets : {
                'yesterday': [
                    {
                        label: 'Screenings',
                        data : [190, 300, 340, 220, 290, 390, 250, 380, 410, 380, 320, 290],
                        fill : 'start'
                    },
                    {
                        label: 'Surgeries',
                        data : [190, 300, 340, 220, 290, 390, 250, 380, 410, 380, 320, 290],
                        fill : 'start'
                    }   
                ],
                'today'    : [
                    {
                        label: 'Screenings',
                        data : [190, 300, 340, 220, 290, 390, 250, 380, 410, 380, 320, 290],
                        fill : 'start'
                    },
                    {
                        label: 'Surgeries',
                        data : [190, 300, 340, 220, 290, 390, 250, 380, 410, 380, 320, 290],
                        fill : 'start'
                    }
                ]
            },
            labels   : ['12am', '2am', '4am', '6am', '8am', '10am', '12pm', '2pm', '4pm', '6pm', '8pm', '10pm'],
            labelsToday: [],
            labelsYesterday: [],
            colors   : [
                {
                    borderColor              : '#3949ab',
                    backgroundColor          : '#3949ab',
                    pointBackgroundColor     : '#3949ab',
                    pointHoverBackgroundColor: '#3949ab',
                    pointBorderColor         : '#ffffff',
                    pointHoverBorderColor    : '#ffffff'
                },
                {
                    borderColor              : 'rgba(30, 136, 229, 0.87)',
                    backgroundColor          : 'rgba(30, 136, 229, 0.87)',
                    pointBackgroundColor     : 'rgba(30, 136, 229, 0.87)',
                    pointHoverBackgroundColor: 'rgba(30, 136, 229, 0.87)',
                    pointBorderColor         : '#ffffff',
                    pointHoverBorderColor    : '#ffffff'
                }
            ],
            options  : {
                spanGaps           : false,
                legend             : {
                    display: false
                },
                maintainAspectRatio: false,
                tooltips           : {
                    position : 'nearest',
                    mode     : 'index',
                    intersect: false
                },
                layout             : {
                    padding: {
                        left : 24,
                        right: 32
                    }
                },
                elements           : {
                    point: {
                        radius          : 4,
                        borderWidth     : 2,
                        hoverRadius     : 4,
                        hoverBorderWidth: 2
                    }
                },
                scales             : {
                    xAxes: [
                        {
                            gridLines: {
                                display: false
                            },
                            ticks    : {
                                fontColor: 'rgba(0,0,0,0.54)'
                            }
                        }
                    ],
                    yAxes: [
                        {
                            gridLines: {
                                tickMarkLength: 16
                            },
                            // ticks    : {
                            //     stepSize: 1000
                            // }
                        }
                    ]
                },
                plugins            : {
                    filler: {
                        propagate: false
                    }
                }
            }
        };
        
        campsConductedMap = {
            markers: [
                
            ],
            bounds : {
                
            },
            styles : [
                {
                    'featureType': 'administrative',
                    'elementType': 'labels.text.fill',
                    'stylers'    : [
                        {
                            'color': '#444444'
                        }
                    ]
                },
                {
                    'featureType': 'landscape',
                    'elementType': 'all',
                    'stylers'    : [
                        {
                            'color': '#f2f2f2'
                        }
                    ]
                },
                {
                    'featureType': 'poi',
                    'elementType': 'all',
                    'stylers'    : [
                        {
                            'visibility': 'off'
                        }
                    ]
                },
                {
                    'featureType': 'road',
                    'elementType': 'all',
                    'stylers'    : [
                        {
                            'saturation': -100
                        },
                        {
                            'lightness': 45
                        }
                    ]
                },
                {
                    'featureType': 'road.highway',
                    'elementType': 'all',
                    'stylers'    : [
                        {
                            'visibility': 'simplified'
                        }
                    ]
                },
                {
                    'featureType': 'road.arterial',
                    'elementType': 'labels.icon',
                    'stylers'    : [
                        {
                            'visibility': 'off'
                        }
                    ]
                },
                {
                    'featureType': 'transit',
                    'elementType': 'all',
                    'stylers'    : [
                        {
                            'visibility': 'off'
                        }
                    ]
                },
                {
                    'featureType': 'water',
                    'elementType': 'all',
                    'stylers'    : [
                        {
                            'color': '#039be5'
                        },
                        {
                            'visibility': 'on'
                        }
                    ]
                }
            ]
        };
        
        vulnerables = {
            scheme : {
                domain: ['#4867d2', '#5c84f1', '#89a9f4', 'a1c9f4']
            },
            days: "7",
            vulnerable: [
                {
                    name  : 'Men',
                    value : 92.8,
                    change: -0.6
                },
                {
                    name  : 'Women',
                    value : 6.1,
                    change: 0.7
                },
                {
                    name  : 'Children',
                    value : 1.1,
                    change: 0.1
                },
                {
                    name  : 'seniors',
                    value : 1.1,
                    change: 0.1
                }
            ]
        };
        
        sales = {
            scheme : {
                domain: ['#5c84f1']
            },
            today  : '12,540',
            change : {
                value     : 321,
                percentage: 2.05
            },
            data   : [
                {
                    name  : 'Sales',
                    series: [
                        {
                            name : 'Jan 1',
                            value: 540
                        },
                        {
                            name : 'Jan 2',
                            value: 539
                        },
                        {
                            name : 'Jan 3',
                            value: 538
                        },
                        {
                            name : 'Jan 4',
                            value: 539
                        },
                        {
                            name : 'Jan 5',
                            value: 540
                        },
                        {
                            name : 'Jan 6',
                            value: 539
                        },
                        {
                            name : 'Jan 7',
                            value: 540
                        }
                    ]
                }
            ],
            dataMin: 538,
            dataMax: 541
        };
        
        topCoordinators = {
            rows: [
                // {
                //     title     : 'Holiday Travel',
                //     screenings    : 3621,
                //     surgeries: 90
                // },
                // {
                //     title     : 'Get Away Deals',
                //     screenings    : 703,
                //     surgeries: 7
                // },
                // {
                //     title     : 'Airfare',
                //     screenings    : 532,
                //     surgeries: 0
                // },
                // {
                //     title     : 'Vacation',
                //     screenings    : 201,
                //     surgeries: 8
                // },
                // {
                //     title     : 'Hotels',
                //     screenings    : 94,
                //     surgeries: 4
                // }
            ]
        };
        
}
