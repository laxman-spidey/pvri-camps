"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var core_1 = require('@angular/core');
var environment_1 = require('../environments/environment');
var http_1 = require('@angular/common/http');
var NetworkService = (function () {
    function NetworkService(http, authService, router) {
        this.http = http;
        this.authService = authService;
        this.router = router;
    }
    NetworkService.prototype.getAPIUrl = function (api) {
        return environment_1.environment.apiUrl + api;
    };
    NetworkService.prototype.post = function (api, postParams) {
        var _this = this;
        if (postParams === void 0) { postParams = {}; }
        var token = this.authService.getToken();
        var httpOptions = {
            headers: new http_1.HttpHeaders({
                'Authorization': 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpZCI6IjkifQ.Zc0CPcFtXSCkgHo9mhg3ogCD_GxjI0hnutFWVv-5FMg'
            })
        };
        console.log('POST :' + this.getAPIUrl(api), httpOptions);
        console.log('POST Params: ', postParams);
        var response = this.http.post(this.getAPIUrl(api), postParams, httpOptions).map(function (res) {
            // if(data.response == 401) {
            //     authService.logout();
            //     return {};
            // } 
            // else {
            //     return res;
            // }
            console.log('POST ' + _this.getAPIUrl(api), res);
            return res;
        });
        return response;
    };
    NetworkService.prototype.get = function (api) {
        var _this = this;
        var token = this.authService.getToken();
        var httpOptions = {
            headers: new http_1.HttpHeaders({
                'Authorization': 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpZCI6IjkifQ.Zc0CPcFtXSCkgHo9mhg3ogCD_GxjI0hnutFWVv-5FMg'
            })
        };
        console.log('GET :' + this.getAPIUrl(api), httpOptions);
        var response = this.http.get(this.getAPIUrl(api), httpOptions).map(function (res) {
            console.log(res);
            // if(data.response == 401) {
            //     authService.logout();
            //     return {};
            // } 
            // else {
            //     return res;
            // }
            console.log('GET ' + _this.getAPIUrl(api), res);
            return res;
        });
        return response;
    };
    NetworkService = __decorate([
        core_1.Injectable()
    ], NetworkService);
    return NetworkService;
}());
exports.NetworkService = NetworkService;
exports.API = {
    // AUTHORIZE : '/pvri/login/validate_credentials',
    AUTHORIZE: '/pvri/auth/token',
    GET_CAMPS_MONTHWISE: '/pvri/camps/getCampsMonthWise',
    GET_SCREENING_SURGERIES: '/pvri/patients/getScreeningsSurgeries',
    GET_EXAMINATIONS: '/pvri/patients/getExaminations',
    GET_GENDER_SCREENINGS: '/pvri/patients/genderWiseScreening',
    // dashboard APIs
    GET_CAMPS_YEARWISE: '/pvri/camps/getCampsYearWise',
    GET_CAMPS_DASHBOARD: '/pvri/camps/getCampsDashboard',
    GET_CAMPS_PAST_WEEK: '/pvri/camps/getCampsPastWeek',
    GET_SCREENINGS_DASHBOARD: '/pvri/camps/getScreeningsDashboard',
    GET_SCREENINGS_PAST_WEEK: '/pvri/patients/getScreeningsPastweek',
    GET_SURGERIES_DASHBOARD: '/pvri/camps/getSurgeriesDashboard',
    GET_SURGERIES_PAST_WEEK: '/pvri/patients/getSurgeriesPastweek',
    GET_VULNERABLES: '/pvri/patients/getVulnerables',
    GET_CAMP_COORDINATORS_PRODUCTIVITY: '/pvri/hierarchy/getCampCoordinatorsProductivity',
    GET_HOURLY_SCREENINGS_SURGERIES: '/pvri/visits/hourlyScreeningsSurgeries',
    // Camp APIs
    GET_PATIENTS: '/pvri/patients/getPatients',
    GET_CAMPS: '/pvri/camps/getCamps',
    // Hierarchy APIs
    GET_STATES: '/pvri/hierarchy/getStates/',
    GET_DISTRICTS: '/pvri/hierarchy/getDistricts/',
    GET_MANDALS: '/pvri/hierarchy/getMandals/',
    GET_LOCATIONS: '/pvri/hierarchy/getLocations/',
    GET_PROJECTS: '/pvri/camps/getProjects/',
    GET_CAMP_COORDINATORS: '/pvri/camps/getCampCoordinators/',
    GET_CAMP_OPTIMETRICIANS: '/pvri/camps/getOptometricAssistants/',
    SCHEDULE_A_CAMP: '/pvri/camps/scheduleACamp',
    UPDATE_CAMP_SCHEDULE: '/pvri/camps/changeCampSchedule',
    CREATE_A_PATIENT: '/pvri/patients/newPatient',
    UPDATE_PATIENT: '/pvri/patients/updatePatient',
    GET_SCREENINGS_OF_PATIENT: '/pvri/patients/getScreeningsPerPatient ',
    UPDATE_PATIENT_PROFILE_PIC: '/pvri/patients/changeProfilePic'
};
