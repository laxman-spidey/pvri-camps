import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { HttpClientModule } from '@angular/common/http';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { RouterModule, Routes } from '@angular/router';
import { TranslateModule } from '@ngx-translate/core';
import 'hammerjs';

import { FuseModule } from '@fuse/fuse.module';
import { FuseSharedModule } from '@fuse/shared.module';

import { fuseConfig } from './fuse-config';

import { AppComponent } from './app.component';
import { FuseMainModule } from './main/main.module';
import { FuseSampleModule } from './main/content/sample/sample.module';
import {LoginModule} from './login/login.module';
import {AuthService} from './login/auth.service';
import {NetworkService} from './network.service';
// import { NewscreeningComponent } from './newscreening/newscreening.component';



const appRoutes: Routes = [
    {
        path      : '**',
        redirectTo: 'dashboard'
    },
    {
        path : 'auth/login',
        redirectTo: 'login'
    }
];

@NgModule({
    declarations: [
        AppComponent
        // NewscreeningComponent
    ],
    imports     : [
        BrowserModule,
        BrowserAnimationsModule,
        HttpClientModule,
        RouterModule.forRoot(appRoutes),
        TranslateModule.forRoot(),

        // Fuse Main and Shared modules
        FuseModule.forRoot(fuseConfig),
        FuseSharedModule,
        FuseMainModule,
        FuseSampleModule,
        LoginModule,
    ],
    bootstrap   : [
        AppComponent
    ],
    providers : [
        AuthService,
        NetworkService
    ]
})
export class AppModule
{
}
