"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var core_1 = require('@angular/core');
var router_1 = require('@angular/router');
var material_1 = require('@angular/material');
var shared_module_1 = require('@fuse/shared.module');
var components_1 = require('@fuse/components');
var content_module_1 = require('app/main/content/content.module');
var footer_module_1 = require('app/main/footer/footer.module');
var navbar_module_1 = require('app/main/navbar/navbar.module');
var quick_panel_module_1 = require('app/main/quick-panel/quick-panel.module');
var toolbar_module_1 = require('app/main/toolbar/toolbar.module');
var main_component_1 = require('./main.component');
var FuseMainModule = (function () {
    function FuseMainModule() {
    }
    FuseMainModule = __decorate([
        core_1.NgModule({
            declarations: [
                main_component_1.FuseMainComponent,
            ],
            imports: [
                router_1.RouterModule,
                material_1.MatSidenavModule,
                shared_module_1.FuseSharedModule,
                components_1.FuseThemeOptionsModule,
                components_1.FuseNavigationModule,
                components_1.FuseSearchBarModule,
                components_1.FuseShortcutsModule,
                components_1.FuseSidebarModule,
                content_module_1.FuseContentModule,
                footer_module_1.FuseFooterModule,
                navbar_module_1.FuseNavbarModule,
                quick_panel_module_1.FuseQuickPanelModule,
                toolbar_module_1.FuseToolbarModule,
            ],
            exports: [
                main_component_1.FuseMainComponent
            ]
        })
    ], FuseMainModule);
    return FuseMainModule;
}());
exports.FuseMainModule = FuseMainModule;
