"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var core_1 = require('@angular/core');
var router_1 = require('@angular/router');
var FuseToolbarComponent = (function () {
    function FuseToolbarComponent(router, fuseConfig, sidebarService, translate, authService) {
        var _this = this;
        this.router = router;
        this.fuseConfig = fuseConfig;
        this.sidebarService = sidebarService;
        this.translate = translate;
        this.authService = authService;
        this.userStatusOptions = [
            {
                'title': 'Online',
                'icon': 'icon-checkbox-marked-circle',
                'color': '#4CAF50'
            },
            {
                'title': 'Away',
                'icon': 'icon-clock',
                'color': '#FFC107'
            },
            {
                'title': 'Do not Disturb',
                'icon': 'icon-minus-circle',
                'color': '#F44336'
            },
            {
                'title': 'Invisible',
                'icon': 'icon-checkbox-blank-circle-outline',
                'color': '#BDBDBD'
            },
            {
                'title': 'Offline',
                'icon': 'icon-checkbox-blank-circle-outline',
                'color': '#616161'
            }
        ];
        this.languages = [
            {
                'id': 'en',
                'title': 'English',
                'flag': 'us'
            },
            {
                'id': 'tr',
                'title': 'Turkish',
                'flag': 'tr'
            }
        ];
        this.selectedLanguage = this.languages[0];
        router.events.subscribe(function (event) {
            if (event instanceof router_1.NavigationStart) {
                _this.showLoadingBar = true;
            }
            if (event instanceof router_1.NavigationEnd) {
                _this.showLoadingBar = false;
            }
        });
        this.fuseConfig.onConfigChanged.subscribe(function (settings) {
            _this.horizontalNav = settings.layout.navigation === 'top';
            _this.noNav = settings.layout.navigation === 'none';
        });
    }
    FuseToolbarComponent.prototype.logout = function () {
        this.authService.logout();
    };
    FuseToolbarComponent.prototype.toggleSidebarOpened = function (key) {
        this.sidebarService.getSidebar(key).toggleOpen();
    };
    FuseToolbarComponent.prototype.search = function (value) {
        // Do your search here...
        console.log(value);
    };
    FuseToolbarComponent.prototype.setLanguage = function (lang) {
        // Set the selected language for toolbar
        this.selectedLanguage = lang;
        // Use the selected language for translations
        this.translate.use(lang.id);
    };
    FuseToolbarComponent = __decorate([
        core_1.Component({
            selector: 'fuse-toolbar',
            templateUrl: './toolbar.component.html',
            styleUrls: ['./toolbar.component.scss']
        })
    ], FuseToolbarComponent);
    return FuseToolbarComponent;
}());
exports.FuseToolbarComponent = FuseToolbarComponent;
