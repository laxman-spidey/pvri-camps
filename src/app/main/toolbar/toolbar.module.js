"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var core_1 = require('@angular/core');
var router_1 = require('@angular/router');
var material_1 = require('@angular/material');
var shared_module_1 = require('@fuse/shared.module');
var toolbar_component_1 = require('app/main/toolbar/toolbar.component');
var components_1 = require('@fuse/components');
var auth_service_1 = require('app/login/auth.service');
var FuseToolbarModule = (function () {
    function FuseToolbarModule() {
    }
    FuseToolbarModule = __decorate([
        core_1.NgModule({
            declarations: [
                toolbar_component_1.FuseToolbarComponent
            ],
            imports: [
                router_1.RouterModule,
                material_1.MatButtonModule,
                material_1.MatIconModule,
                material_1.MatMenuModule,
                material_1.MatProgressBarModule,
                material_1.MatToolbarModule,
                shared_module_1.FuseSharedModule,
                components_1.FuseSearchBarModule,
                components_1.FuseShortcutsModule
            ],
            providers: [auth_service_1.AuthService],
            exports: [
                toolbar_component_1.FuseToolbarComponent
            ]
        })
    ], FuseToolbarModule);
    return FuseToolbarModule;
}());
exports.FuseToolbarModule = FuseToolbarModule;
