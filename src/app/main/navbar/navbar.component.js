"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var core_1 = require('@angular/core');
var fuse_perfect_scrollbar_directive_1 = require('@fuse/directives/fuse-perfect-scrollbar/fuse-perfect-scrollbar.directive');
var navigation_1 = require('app/navigation/navigation');
var FuseNavbarComponent = (function () {
    function FuseNavbarComponent(sidebarService, navigationService) {
        this.sidebarService = sidebarService;
        this.navigationService = navigationService;
        // Navigation data
        this.navigation = navigation_1.navigation;
        // Default layout
        this.layout = 'vertical';
    }
    Object.defineProperty(FuseNavbarComponent.prototype, "directive", {
        set: function (theDirective) {
            var _this = this;
            if (!theDirective) {
                return;
            }
            this.fusePerfectScrollbar = theDirective;
            this.navigationServiceWatcher =
                this.navigationService.onItemCollapseToggled.subscribe(function () {
                    _this.fusePerfectScrollbarUpdateTimeout = setTimeout(function () {
                        _this.fusePerfectScrollbar.update();
                    }, 310);
                });
        },
        enumerable: true,
        configurable: true
    });
    FuseNavbarComponent.prototype.ngOnDestroy = function () {
        if (this.fusePerfectScrollbarUpdateTimeout) {
            clearTimeout(this.fusePerfectScrollbarUpdateTimeout);
        }
        if (this.navigationServiceWatcher) {
            this.navigationServiceWatcher.unsubscribe();
        }
    };
    FuseNavbarComponent.prototype.toggleSidebarOpened = function (key) {
        this.sidebarService.getSidebar(key).toggleOpen();
    };
    FuseNavbarComponent.prototype.toggleSidebarFolded = function (key) {
        this.sidebarService.getSidebar(key).toggleFold();
    };
    __decorate([
        core_1.ViewChild(fuse_perfect_scrollbar_directive_1.FusePerfectScrollbarDirective)
    ], FuseNavbarComponent.prototype, "directive");
    __decorate([
        core_1.Input()
    ], FuseNavbarComponent.prototype, "layout");
    FuseNavbarComponent = __decorate([
        core_1.Component({
            selector: 'fuse-navbar',
            templateUrl: './navbar.component.html',
            styleUrls: ['./navbar.component.scss'],
            encapsulation: core_1.ViewEncapsulation.None
        })
    ], FuseNavbarComponent);
    return FuseNavbarComponent;
}());
exports.FuseNavbarComponent = FuseNavbarComponent;
