"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var core_1 = require('@angular/core');
var router_1 = require('@angular/router');
var material_1 = require('@angular/material');
var shared_module_1 = require('@fuse/shared.module');
var navbar_component_1 = require('app/main/navbar/navbar.component');
var components_1 = require('@fuse/components');
var FuseNavbarModule = (function () {
    function FuseNavbarModule() {
    }
    FuseNavbarModule = __decorate([
        core_1.NgModule({
            declarations: [
                navbar_component_1.FuseNavbarComponent
            ],
            imports: [
                router_1.RouterModule,
                material_1.MatButtonModule,
                material_1.MatIconModule,
                shared_module_1.FuseSharedModule,
                components_1.FuseNavigationModule
            ],
            exports: [
                navbar_component_1.FuseNavbarComponent
            ]
        })
    ], FuseNavbarModule);
    return FuseNavbarModule;
}());
exports.FuseNavbarModule = FuseNavbarModule;
