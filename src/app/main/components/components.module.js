"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var core_1 = require('@angular/core');
var router_1 = require('@angular/router');
var material_1 = require('@angular/material');
var ngx_charts_1 = require('@swimlane/ngx-charts');
var components_1 = require('@fuse/components');
// import { FuseAngularMaterialModule } from './angular-material/angular-material.module';
var cards_component_1 = require('./cards/cards.component');
// import { FuseCountdownDocsComponent } from './countdown/countdown.component';
// import { FuseHighlightDocsComponent } from './highlight/highlight.component';
// import { FuseMaterialColorPickerDocsComponent } from './material-color-picker/material-color-picker.component';
// import { FuseMultiLanguageDocsComponent } from './multi-language/multi-language.component';
// import { FuseNavigationDocsComponent } from './navigation/navigation.component';
// import { FuseSearchBarDocsComponent } from './search-bar/search-bar.component';
// import { FuseSidebarDocsComponent } from './sidebar/sidebar.component';
// import { FuseShortcutsDocsComponent } from './shortcuts/shortcuts.component';
var widget_component_1 = require('./widget/widget.component');
var routes = [
    {
        path: 'cards',
        component: cards_component_1.FuseCardsDocsComponent
    },
    // {
    //     path     : 'countdown',
    //     component: FuseCountdownDocsComponent
    // },
    // {
    //     path     : 'highlight',
    //     component: FuseHighlightDocsComponent
    // },
    // {
    //     path     : 'material-color-picker',
    //     component: FuseMaterialColorPickerDocsComponent
    // },
    // {
    //     path     : 'multi-language',
    //     component: FuseMultiLanguageDocsComponent
    // },
    // {
    //     path     : 'navigation',
    //     component: FuseNavigationDocsComponent
    // },
    // {
    //     path     : 'search-bar',
    //     component: FuseSearchBarDocsComponent
    // },
    // {
    //     path     : 'sidebar',
    //     component: FuseSidebarDocsComponent
    // },
    // {
    //     path     : 'shortcuts',
    //     component: FuseShortcutsDocsComponent
    // },
    {
        path: 'widget',
        component: widget_component_1.FuseWidgetDocsComponent
    }
];
var FuseComponentsModule = (function () {
    function FuseComponentsModule() {
    }
    FuseComponentsModule = __decorate([
        core_1.NgModule({
            declarations: [
                cards_component_1.FuseCardsDocsComponent,
                // FuseCountdownDocsComponent,
                // FuseHighlightDocsComponent,
                // FuseMaterialColorPickerDocsComponent,
                // FuseMultiLanguageDocsComponent,
                // FuseNavigationDocsComponent,
                // FuseSearchBarDocsComponent,
                // FuseSidebarDocsComponent,
                // FuseShortcutsDocsComponent,
                widget_component_1.FuseWidgetDocsComponent
            ],
            imports: [
                router_1.RouterModule.forChild(routes),
                material_1.MatButtonModule,
                material_1.MatButtonToggleModule,
                material_1.MatFormFieldModule,
                material_1.MatIconModule,
                material_1.MatListModule,
                material_1.MatMenuModule,
                material_1.MatSelectModule,
                material_1.MatSlideToggleModule,
                material_1.MatTabsModule,
                ngx_charts_1.NgxChartsModule,
                // FuseSharedModule,
                // FuseCountdownModule,
                // FuseHighlightModule,
                // FuseMaterialColorPickerModule,
                components_1.FuseWidgetModule,
            ]
        })
    ], FuseComponentsModule);
    return FuseComponentsModule;
}());
exports.FuseComponentsModule = FuseComponentsModule;
