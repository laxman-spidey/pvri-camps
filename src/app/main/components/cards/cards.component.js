"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var core_1 = require('@angular/core');
var shape = require('d3-shape');
var animations_1 = require('@fuse/animations');
var FuseCardsDocsComponent = (function () {
    function FuseCardsDocsComponent() {
        this.view = 'preview';
        // Card 9
        this.card9Expanded = false;
        // Card 10
        this.card10Expanded = false;
        // Card 19
        this.card19 = {
            scheme: {
                domain: ['#5c84f1']
            },
            data: [
                {
                    'name': 'GOOG',
                    'series': [
                        {
                            'name': 'Jan 1',
                            'value': 540.2
                        },
                        {
                            'name': 'Jan 2',
                            'value': 539.4
                        },
                        {
                            'name': 'Jan 3',
                            'value': 538.9
                        },
                        {
                            'name': 'Jan 4',
                            'value': 539.6
                        },
                        {
                            'name': 'Jan 5',
                            'value': 540
                        },
                        {
                            'name': 'Jan 6',
                            'value': 540.2
                        },
                        {
                            'name': 'Jan 7',
                            'value': 540.48
                        }
                    ]
                }
            ],
            curve: shape.curveBasis
        };
        // Card 24
        this.card24 = {
            scheme: {
                domain: ['#4867d2', '#5c84f1', '#89a9f4']
            },
            devices: [
                {
                    'name': 'Desktop',
                    'value': 92.8,
                    'change': -0.6
                },
                {
                    'name': 'Mobile',
                    'value': 6.1,
                    'change': 0.7
                },
                {
                    'name': 'Tablet',
                    'value': 1.1,
                    'change': 0.1
                }
            ]
        };
        // Card 25
        this.card25 = {
            scheme: {
                domain: ['#5c84f1']
            },
            data: [
                {
                    'name': 'Monday',
                    'value': 221
                },
                {
                    'name': 'Tuesday',
                    'value': 428
                },
                {
                    'name': 'Wednesday',
                    'value': 492
                },
                {
                    'name': 'Thursday',
                    'value': 471
                },
                {
                    'name': 'Friday',
                    'value': 413
                },
                {
                    'name': 'Saturday',
                    'value': 344
                },
                {
                    'name': 'Sunday',
                    'value': 294
                }
            ]
        };
        // Card 26
        this.card26 = {
            scheme: {
                domain: ['#5c84f1']
            },
            data: [
                {
                    'name': 'Impressions',
                    'series': [
                        {
                            'name': 'Jan 1',
                            'value': 670000
                        },
                        {
                            'name': 'Jan 2',
                            'value': 540000
                        },
                        {
                            'name': 'Jan 3',
                            'value': 820000
                        },
                        {
                            'name': 'Jan 4',
                            'value': 570000
                        },
                        {
                            'name': 'Jan 5',
                            'value': 720000
                        },
                        {
                            'name': 'Jan 6',
                            'value': 570000
                        },
                        {
                            'name': 'Jan 7',
                            'value': 870000
                        },
                        {
                            'name': 'Jan 8',
                            'value': 720000
                        },
                        {
                            'name': 'Jan 9',
                            'value': 890000
                        },
                        {
                            'name': 'Jan 10',
                            'value': 987000
                        },
                        {
                            'name': 'Jan 11',
                            'value': 1120000
                        },
                        {
                            'name': 'Jan 12',
                            'value': 1360000
                        },
                        {
                            'name': 'Jan 13',
                            'value': 1100000
                        },
                        {
                            'name': 'Jan 14',
                            'value': 1490000
                        },
                        {
                            'name': 'Jan 15',
                            'value': 980000
                        }
                    ]
                }
            ],
            curve: shape.curveBasis
        };
    }
    FuseCardsDocsComponent.prototype.toggleView = function () {
        if (this.view === 'preview') {
            this.view = 'source';
        }
        else {
            this.view = 'preview';
        }
    };
    FuseCardsDocsComponent = __decorate([
        core_1.Component({
            selector: 'fuse-cards-docs',
            templateUrl: './cards.component.html',
            styleUrls: ['./cards.component.scss'],
            animations: animations_1.fuseAnimations
        })
    ], FuseCardsDocsComponent);
    return FuseCardsDocsComponent;
}());
exports.FuseCardsDocsComponent = FuseCardsDocsComponent;
