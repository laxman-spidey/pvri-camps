import { NewcampComponent } from './newcamp/newcamp.component';
import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs/Observable';
import {of as observableOf} from 'rxjs/observable/of';
import { NetworkService, API} from 'app/network.service';
import { MockModels} from 'app/mock.models';
import { Camp } from './camp.model';
import { Patient } from './patients/newpatient/newpatient.model';
import { Screening } from './patients/newscreening/newscreening.model';


@Injectable()
export class CampsService {

  private mockModels: MockModels;
  constructor(private networkService: NetworkService) { 
    this.mockModels = new MockModels();
  }
  
  getCamps(): Observable<any> {
        return this.networkService.get(API.GET_CAMPS);        
        //  return observableOf(this.mockModels.camps);
    }
    
    getPatients(campId: string): Observable<any> {
      return this.networkService.post(API.GET_PATIENTS, {cid: campId});
      // return  observableOf(this.mockModels.patients);
      
    }

    scheduleACamp(newCamp: Camp): Observable<any> {
      const camp = new Camp(newCamp);
      camp.camp_id = undefined;
      camp.location_id = undefined;
      camp.mandal_code = undefined;
      
      const postData = {
        camp: JSON.stringify(camp)
      };
      return this.networkService.post(API.SCHEDULE_A_CAMP, postData);
    }
    updateCampSchedule(editedCamp: Camp): Observable<any> {
      const postData = {
        camp: JSON.stringify(editedCamp.getEditableCamp())
      };
      return this.networkService.post(API.UPDATE_CAMP_SCHEDULE, postData);
    }
    createAPatient(patient: Patient, picture: string | undefined): Observable<any> {
      patient.patient_id = undefined;
      const postData = {
        patient: JSON.stringify(patient),
        picture: picture
      };
      return this.networkService.post(API.CREATE_A_PATIENT, postData);
    }
    
    updatePatient(patient: Patient): Observable<any> {
      const postData = {
        camp: JSON.stringify(patient)
      };
      return this.networkService.post(API.UPDATE_PATIENT, postData);
    }
    getScreeningsOfPatient(patientId: number) {
      const postData = {
        pid: patientId
      };
      return this.networkService.post(API.GET_SCREENINGS_OF_PATIENT, postData);
    }
    updatePatientProfilePic(picBase64: string) {
      const postData = {
        picture: picBase64
      };
      return this.networkService.post(API.GET_SCREENINGS_OF_PATIENT, postData);
    }
    createAScreening(screening: Screening): Observable<any> {
      const postData = {
        camp: JSON.stringify(screening)
      };
      return this.networkService.post(API.CREATE_A_SCREENING, postData);
    }
}
