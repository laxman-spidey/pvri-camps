import { Subscription } from 'rxjs/Subscription';
import {Component, OnInit, ViewChild, Pipe, PipeTransform,Injectable ,Inject } from '@angular/core';
import {MatPaginator, MatSort, MatTableDataSource} from '@angular/material';
import {merge} from 'rxjs/observable/merge';
import {of as observableOf} from 'rxjs/observable/of';
import {catchError} from 'rxjs/operators/catchError';
import {map} from 'rxjs/operators/map';
import {startWith} from 'rxjs/operators/startWith';
import {switchMap} from 'rxjs/operators/switchMap';
import {CampsService} from './camps.service';
import { NewcampComponent } from './newcamp/newcamp.component';
import {MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material';
import { Router } from '@angular/router';
import { fuseAnimations } from '@fuse/animations';
import { Camp } from './camp.model';


@Component({
  selector: 'app-camps',
  templateUrl: './camps.component.html',
  styleUrls: ['./camps.component.scss'],
  animations   : [fuseAnimations]
    
})
export class CampsComponent implements OnInit {
  camps: any = {
      columns: ['camp_id', 'camp_place', 'camp_date', 'state_name', 'district_name', 'mandal_name', 'coordinator_name', 'opthalmologist_name', 'screenings', 'Edit'],
      rows: [],
  };
  isLoadingResults: boolean;
  dataSourceOriginal: any = {};
  campsDataSource = new MatTableDataSource();
  onCampUpdated: Subscription;
  onCampListUpdated: Subscription;

  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;
  constructor(
                private campsService: CampsService,
                public dialog: MatDialog,
                private router: Router,
                ) { }

  ngOnInit() {
    
   
    this.isLoadingResults = true;
    this.campsService.getCamps().subscribe(data => {
      this.isLoadingResults = false;
      console.log(data.data);
      this.campsDataSource.data = data.data; 
      this.dataSourceOriginal.data = data.data;
      this.camps.rows = this.campsDataSource.data;
      console.log(this.campsDataSource.data);
      
      console.log(this.isLoadingResults);
    });
    
    
    
    
    // this.sort.sortChange.subscribe(() => this.paginator.pageIndex = 0);

  //   merge(this.sort.sortChange, this.paginator.page)
  //     .pipe(
  //       startWith({}),
  //       switchMap(() => {
  //         this.isLoadingResults = true;
  //         // return this.exampleDatabase!.getRepoIssues(
  //         //   this.sort.active, this.sort.direction, this.paginator.pageIndex);
  //         return this.campsService!.getCamps();
  //       }),
  //       map(data => {
  //         // Flip flag to show that loading has finished.
  //         this.isLoadingResults = false;
  //         return data;
  //       }),
  //       catchError((error) => {
  //         this.isLoadingResults = false;
  //         console.log(error);
  //         return observableOf({});
  //       })
  //     ).subscribe(data => {  
  //       this.campsDataSource.data = JSON.parse(data); 
  //       this.dataSourceOriginal.data = JSON.parse(data);
  //       // this.dataSource.data = data; 
  //       console.log("received data");
  //       console.log(this.campsDataSource.data);
  //     });
  }
  
 
  campClicked(camp: any) {
    console.log(camp);
    this.router.navigateByUrl('/camps/' + camp.camp_id + '/patients');  
  }

  addCamp(){
    const dialogRef = this.dialog.open(NewcampComponent, {
      panelClass: 'contact-form-dialog',
      data : {
        action: 'new'
      }
    });

    dialogRef.afterClosed().subscribe(result => {
      this.campsDataSource.data.push(result.camp);
      this.campsDataSource.data = JSON.parse(JSON.stringify(this.campsDataSource.data));
      console.log(result);
      console.log(this.campsDataSource.data);
    });

  }
  
  editCamp(camp: Camp) {
    const dialogRef = this.dialog.open(NewcampComponent, {
      panelClass: 'contact-form-dialog',
      data : {
        action: 'edit',
        camp: camp
      }
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result !== undefined) {
        result.camp.mandal_code = result.camp.mandal_id;
        const camps = this.camps.rows;
        for (const key in camps) {
          if (camps.hasOwnProperty(key)) {
            if (result.camp.camp_id === camps[key].camp_id) 
            {
              camps[key] = result.camp;
              break;
            }
          }
        }
      }
      console.log(this.campsDataSource.data);
      // this.campsDataSource.data;
    });
  }
}
