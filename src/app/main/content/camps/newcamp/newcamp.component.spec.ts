import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NewcampComponent } from './newcamp.component';

describe('NewcampComponent', () => {
  let component: NewcampComponent;
  let fixture: ComponentFixture<NewcampComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NewcampComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NewcampComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
