"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
var core_1 = require('@angular/core');
var forms_1 = require('@angular/forms');
var material_1 = require('@angular/material');
var network_service_1 = require('app/network.service');
var newcamp_model_1 = require('./newcamp.model');
var Observable_1 = require('rxjs/Observable');
require('rxjs/add/observable/forkJoin');
var animations_1 = require('@fuse/animations');
var NewcampComponent = (function () {
    function NewcampComponent(dialogRef, data, formBuilder, http, networkService, campsService) {
        var _this = this;
        this.dialogRef = dialogRef;
        this.data = data;
        this.formBuilder = formBuilder;
        this.http = http;
        this.networkService = networkService;
        this.campsService = campsService;
        this.isLoadingResults = false;
        this.newCamp = {
            state: '',
            district: '',
            mandal: ''
        };
        this.states = [];
        this.districts = [];
        this.mandals = [];
        this.projects = [];
        this.locations = [];
        this.locationsOriginal = [];
        this.campCoordinators = [];
        this.statesOriginal = [];
        this.districtsOriginal = [];
        this.mandalsOriginal = [];
        this.opthalmologists = [];
        this.selectedDistrict = '';
        this.selectedLocation = '';
        this.initMapData();
        this.action = data.action;
        console.log('action', data.camp);
        if (this.action === 'edit') {
            this.dialogTitle = 'Edit Camp';
            console.log('Camp data: ', data.camp);
            this.camp = data.camp;
            this.selectedDistrict = this.camp.district_id;
        }
        else {
            this.dialogTitle = 'Schedule a New Camp';
            // this.patient = this.patient;
            this.camp = new newcamp_model_1.Camp();
        }
        this.isLoadingResults = true;
        Observable_1.Observable.forkJoin(this.networkService.get(network_service_1.API.GET_STATES), this.networkService.get(network_service_1.API.GET_DISTRICTS), this.networkService.get(network_service_1.API.GET_MANDALS), this.networkService.get(network_service_1.API.GET_LOCATIONS), this.networkService.get(network_service_1.API.GET_PROJECTS), this.networkService.get(network_service_1.API.GET_CAMP_COORDINATORS), this.networkService.get(network_service_1.API.GET_CAMP_OPTIMETRICIANS)).subscribe(function (response) {
            _this.states = response[0].data;
            _this.statesOriginal = response[0].data;
            console.log(_this.states);
            _this.districts = response[1].data;
            _this.districtsOriginal = response[1].data;
            console.log(JSON.stringify(_this.districts));
            _this.mandals = response[2].data;
            _this.mandalsOriginal = response[2].data;
            console.log(JSON.stringify(_this.mandals));
            _this.locations = response[3].data;
            _this.locationsOriginal = response[3].data;
            _this.projects = response[4].data;
            _this.campCoordinators = response[5].data;
            _this.opthalmologists = response[6].data;
            _this.isLoadingResults = false;
        });
        this.newCampForm = this.formBuilder.group({
            state: [this.camp.state_id, forms_1.Validators.required],
            district: [this.selectedDistrict, forms_1.Validators.required],
            mandal: [this.camp.mandal_id, forms_1.Validators.required],
            location: [this.camp.location_id],
            campDate: [this.camp.camp_date, forms_1.Validators.required],
            project: [this.camp.project_id, forms_1.Validators.required],
            campCoordinator: [this.camp.camp_coordinator_id, forms_1.Validators.required],
            campOpthamologist: [this.camp.camp_opthalmologist_id, forms_1.Validators.required],
            campPlace: [this.camp.camp_place, forms_1.Validators.required]
        });
        // forkJoin([stateObservable, districtObservable]).subscribe((results) =>{
        //     console.log('both loaded');
        // });
    }
    NewcampComponent.prototype.scheduleACamp = function () {
        var _this = this;
        var camp = new newcamp_model_1.Camp(this.camp.camp_id, this.newCampForm.get('campDate').value, this.newCampForm.get('location').value, this.newCampForm.get('project').value, this.newCampForm.get('mandal').value, this.newCampForm.get('campCoordinator').value, this.newCampForm.get('campOpthamologist').value, this.newCampForm.get('campPlace').value, this.mapData.marker.lat, this.mapData.marker.long);
        if (this.action === 'edit') {
            this.campsService.updateCampSchedule(camp).subscribe(function (response) {
                _this.dialogRef.close(['save', camp]);
            });
        }
        else {
            this.campsService.scheduleACamp(camp).subscribe(function (response) {
                camp.camp_id = response.data.camp_id;
                _this.dialogRef.close(['save', camp]);
            });
        }
    };
    NewcampComponent.prototype.onMapReady = function (map) {
        this.map = map;
    };
    NewcampComponent.prototype.initMapData = function () {
        this.mapData = {
            marker: {
                lat: 17.387140,
                long: 78.491684,
                draggable: true,
                label: 'Camp Location'
            },
            zoom: 14
        };
    };
    NewcampComponent.prototype.locateAddress = function () {
        var _this = this;
        this.geocoder = new google.maps.Geocoder();
        var address = this.newCampForm.get('campPlace').value;
        this.geocoder.geocode({
            'address': address
        }, function (results, status) {
            if (status === google.maps.GeocoderStatus.OK) {
                _this.mapData.marker.lat = results[0].geometry.location.lat();
                _this.mapData.marker.long = results[0].geometry.location.lng();
            }
            else {
                alert('Geocode was not successful for the following reason: ' + status);
            }
        });
    };
    NewcampComponent.prototype.onMapClicked = function ($event) {
        // this.mapData.marker = {
        //   lat: $event.coords.lat,
        //   long: $event.coords.lng,
        //   draggable: true,
        //   label: 'Camp Location'
        // };
    };
    NewcampComponent.prototype.markerDragEnd = function (m, $event) {
        console.log('dragEnd', m, $event);
        this.mapData.marker = {
            lat: $event.coords.lat,
            long: $event.coords.lng,
            draggable: true,
            label: 'Camp Location'
        };
    };
    NewcampComponent.prototype.onStateSelect = function ($event) {
        this.districts = this.districtsOriginal.filter(function (district) { return district.state_code === $event.value; });
        this.selectedDistrict = '';
        this.mandals = [];
    };
    NewcampComponent.prototype.onDistrictSelect = function ($event) {
        this.mandals = this.mandalsOriginal.filter(function (mandal) { return mandal.district_code === $event.value; });
    };
    NewcampComponent.prototype.onProjectSelected = function ($event) {
        this.locations = this.locationsOriginal.filter(function (location) { return location.project_id = $event.value; });
    };
    NewcampComponent = __decorate([
        core_1.Component({
            selector: 'app-newcamp',
            templateUrl: './newcamp.component.html',
            styleUrls: ['./newcamp.component.scss'],
            encapsulation: core_1.ViewEncapsulation.None,
            animations: animations_1.fuseAnimations
        }),
        __param(1, core_1.Inject(material_1.MAT_DIALOG_DATA))
    ], NewcampComponent);
    return NewcampComponent;
}());
exports.NewcampComponent = NewcampComponent;
