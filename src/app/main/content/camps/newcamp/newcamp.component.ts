import { state } from '@angular/animations';
import { CampsService } from './../camps.service';
import { Component, Inject, ViewEncapsulation, ViewChild, ElementRef  } from '@angular/core';
import { FormBuilder, FormGroup , FormControl, Validators} from '@angular/forms';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material';
import {HttpClient} from '@angular/common/http';
import {NetworkService, API} from 'app/network.service';
import {} from '@types/googlemaps';
import { CalendarEvent } from 'angular-calendar';

import { Camp } from '../camp.model';

import { Observable } from 'rxjs/Observable';
import 'rxjs/add/observable/forkJoin';
import { fuseAnimations } from '@fuse/animations';


@Component({
    selector: 'app-newcamp',
    templateUrl: './newcamp.component.html',
    styleUrls: ['./newcamp.component.scss'],
    encapsulation: ViewEncapsulation.None,
    animations   : fuseAnimations
})

export class NewcampComponent
{
    camp: Camp;
    isLoadingResults = false;
    newCamp = {
        state: '',
        district: '',
        mandal: '',
    };
    map: google.maps.Map;
    geocoder: any;
    mapData: any;
    newCampForm: FormGroup;
    event: CalendarEvent;
    dialogTitle: string;
    contactForm: FormGroup;
    action: string;
    // contact: Contact;
    id: string;
    
    states = [];
    districts = [];
    mandals = [];
    
    projects = [];
    locations = [];
    locationsOriginal = [];
    campCoordinators = [];
    statesOriginal = [];
    districtsOriginal = [];
    mandalsOriginal = [];
    opthalmologists = [];

    
    selectedDistrict = '';
    selectedLocation = '';

    constructor(
        public dialogRef: MatDialogRef<NewcampComponent>,
        @Inject(MAT_DIALOG_DATA) private data: any,
        private formBuilder: FormBuilder,
        private http: HttpClient, 
        private networkService: NetworkService,
        private campsService: CampsService,
    )
    {
        this.initMapData();
        this.action = data.action;
        console.log('action', data.camp);
        if ( this.action === 'edit' )
        {
            this.dialogTitle = 'Edit Camp';
            console.log('Camp data: ', data.camp);
            this.camp = new Camp(data.camp);
            this.selectedDistrict = this.camp.district_code;
        }
        else
        {
            this.dialogTitle = 'Schedule a New Camp';
            // this.patient = this.patient;
            this.camp = new Camp();
            
        }


        this.isLoadingResults = true;
        Observable.forkJoin(
        this.networkService.get(API.GET_STATES),
        this.networkService.get(API.GET_DISTRICTS),
        this.networkService.get(API.GET_MANDALS),
        this.networkService.get(API.GET_LOCATIONS),
        this.networkService.get(API.GET_PROJECTS),
        this.networkService.get(API.GET_CAMP_COORDINATORS),
        this.networkService.get(API.GET_CAMP_OPTIMETRICIANS)
        ).subscribe(response => {
            this.states = <any>response[0].data;
            this.statesOriginal = response[0].data;
            console.log(this.states);
            
            this.districts = response[1].data;
            this.districtsOriginal = response[1].data;
            console.log(JSON.stringify(this.districts));
            
            this.mandals = response[2].data;
            this.mandalsOriginal = response[2].data;
            console.log(JSON.stringify(this.mandals));

            this.locations = response[3].data;
            this.locationsOriginal = response[3].data;
            this.projects = response[4].data;
            this.campCoordinators = response[5].data;
            this.opthalmologists = response[6].data;
            this.isLoadingResults = false;
        });

        this.newCampForm = this.formBuilder.group({
            state: [this.camp.state_code, Validators.required],
            district: [this.selectedDistrict, Validators.required],
            mandal: [this.camp.mandal_code, Validators.required],
            location: [this.camp.location_id],
            campDate: [this.camp.camp_date, Validators.required],
            project: [this.camp.project_id, Validators.required],
            campCoordinator: [this.camp.camp_coordinator_id, Validators.required],
            campOpthamologist: [this.camp.camp_opthalmologist_id, Validators.required],
            campPlace: [this.camp.camp_place, Validators.required],
            
        });
        
        // forkJoin([stateObservable, districtObservable]).subscribe((results) =>{
        //     console.log('both loaded');
        // });
        
    }
    
    scheduleACamp() {
        this.camp.camp_id = this.camp.camp_id;
        this.camp.setDate(this.newCampForm.get('campDate').value);
        this.camp.location_id = this.newCampForm.get('location').value;
        this.camp.project_id = this.newCampForm.get('project').value;
        this.camp.mandal_code = this.newCampForm.get('mandal').value;
        this.camp.mandal_id = this.newCampForm.get('mandal').value;
        this.camp.camp_coordinator_id = this.newCampForm.get('campCoordinator').value;
        this.camp.camp_opthalmologist_id = this.newCampForm.get('campOpthamologist').value;
        this.camp.camp_place = this.newCampForm.get('campPlace').value;
        this.camp.latitude = this.mapData.marker.lat;
        this.camp.longitude = this.mapData.marker.long;
        
        if (this.action === 'edit') {  
            this.campsService.updateCampSchedule(this.camp).subscribe((response) => {        
                this.dialogRef.close({action: this.action, camp: this.camp});
           });
        }
        else {
            this.campsService.scheduleACamp(this.camp).subscribe((response) => {  
                this.camp.camp_id = response.data.camp_id;
                this.dialogRef.close({action: this.action, camp: this.camp});
            });
        }
    }
    onMapReady(map) {
        this.map = map;
    }
    
    initMapData() {
        this.mapData = {
            marker: {
                lat: 17.387140,
                long: 78.491684,
                draggable: true,
                label: 'Camp Location'
            },
            zoom: 14
        }
        ;
    }
    
    locateAddress() {
        this.geocoder = new google.maps.Geocoder();
        const address = this.newCampForm.get('campPlace').value;
        this.geocoder.geocode({
          'address': address
        }, (results, status) => {
          if (status === google.maps.GeocoderStatus.OK) {
            this.mapData.marker.lat = results[0].geometry.location.lat();
            this.mapData.marker.long = results[0].geometry.location.lng();            
          } else {
            alert('Geocode was not successful for the following reason: ' + status);
          }
        });
      }

    onMapClicked($event: MouseEvent) {
        // this.mapData.marker = {
        //   lat: $event.coords.lat,
        //   long: $event.coords.lng,
        //   draggable: true,
        //   label: 'Camp Location'
        // };
      }
    
    markerDragEnd(m: google.maps.Marker, $event) {
        console.log('dragEnd', m, $event);
        this.mapData.marker = {
          lat: $event.coords.lat,
          long: $event.coords.lng,
          draggable: true,
          label: 'Camp Location'
        };
        
    }

    onStateSelect($event) {
        this.districts = this.districtsOriginal.filter((district) => district.state_code === $event.value);
        this.selectedDistrict = '';
        this.mandals = [];
    }
    onDistrictSelect($event) {
        this.mandals = this.mandalsOriginal.filter((mandal) => mandal.district_code === $event.value);
    }
    onProjectSelected($event) {
        this.locations = this.locationsOriginal.filter((location) => location.project_id = $event.value);
    }
}
