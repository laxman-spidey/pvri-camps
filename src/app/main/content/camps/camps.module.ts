import { NgModule, Pipe, PipeTransform } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CampsComponent } from './camps.component';
import { NetworkService } from 'app/network.service';
import { RouterModule, Routes } from '@angular/router';
import {CampsService} from './camps.service';
import { FuseSharedModule } from '@fuse/shared.module';
import {CdkTableModule} from '@angular/cdk/table';
import {FlexLayoutModule} from '@angular/flex-layout';
import { FuseWidgetModule } from '@fuse/components/widget/widget.module';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import {NewpatientComponent} from './patients/newpatient/newpatient.component';
import { BrowserModule } from '@angular/platform-browser';
import { AgmCoreModule, GoogleMapsAPIWrapper, MapsAPILoader } from '@agm/core';
import {MatStepperModule} from '@angular/material/stepper';


import {
    MatCardModule,
    MatPaginatorModule,
    MatTableModule,
    MatButtonModule,
    MatIconModule,
    MatInputModule,
    MatFormFieldModule,
    MatDialogModule,
    MatDatepickerModule,
    MatNativeDateModule,
    MatProgressBarModule,
    MatProgressSpinnerModule,
    MatRippleModule,
    MatSortModule,
    MatListModule,
    MatSelectModule,
    MatToolbarModule,
    MatSlideToggleModule
} from '@angular/material';

import { NewcampComponent } from './newcamp/newcamp.component';
import { HttpModule } from '@angular/http';
import { PatientsComponent } from './patients/patients.component';
import { ScreeningsComponent } from './patients/screenings/screenings.component';
import { VisitsComponent } from './patients/visits/visits.component';
import { NewscreeningComponent } from './patients/newscreening/newscreening.component';

// import {MomentDateAdapter} from '@angular/material-moment-adapter';
// import {DateAdapter, MAT_DATE_FORMATS, MAT_DATE_LOCALE} from '@angular/material/core';


const routes: Routes = [
    {
        path     : 'camps',
        redirectTo : 'camps/list',
    },
    {
        path     : 'camps/list',
        component: CampsComponent,
    },
    {
        path      : 'camps/newcamp',
        component : NewcampComponent,
    },
    {
        path      : 'camps/:campid/patients',
        component : PatientsComponent,
    }
];

@NgModule({
  imports: [  
    BrowserAnimationsModule,
    CommonModule,
    RouterModule.forChild(routes),
    FuseSharedModule,
    CdkTableModule,
    FuseWidgetModule,
    HttpModule,
    // mat modules
    MatPaginatorModule,
    MatCardModule,
    MatTableModule,
    MatButtonModule,
    MatIconModule,
    MatInputModule,
    MatFormFieldModule,
    MatDialogModule,
    MatDatepickerModule,
    MatNativeDateModule,
    MatProgressBarModule,
    MatProgressSpinnerModule,
    MatRippleModule,
    MatSortModule,
    MatListModule,
    MatSelectModule,
    MatToolbarModule,
    MatSlideToggleModule,
    MatStepperModule,
    AgmCoreModule.forRoot({
        apiKey: 'AIzaSyCnDIHnV2BMiSRAZ2l_pnWKsf0WH-eJ7CM',
        libraries: ['geometry']
    }),
    
  ],
  entryComponents : [NewcampComponent, NewpatientComponent,NewscreeningComponent],
  providers: [NetworkService, CampsService, GoogleMapsAPIWrapper],
  declarations: [CampsComponent, NewcampComponent, PatientsComponent, NewpatientComponent, ScreeningsComponent, VisitsComponent,NewscreeningComponent]
})
export class CampsModule { }
