"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var core_1 = require('@angular/core');
var material_1 = require('@angular/material');
var newpatient_component_1 = require('./newpatient/newpatient.component');
var animations_1 = require('@angular/animations');
var PatientsComponent = (function () {
    function PatientsComponent(route, router, campsService, dialog) {
        this.route = route;
        this.router = router;
        this.campsService = campsService;
        this.dialog = dialog;
        this.isLoadingResults = false;
        this.patientData = null;
        this.displayedColumns = ['patient_id', 'patient_name', 'HEAD_OF_FAMILY_NAME', 'AADHAR', 'AGE', 'CONTACT_NUMBER', 'GENDER', 'BELOW_POVERTY_LINE', 'WHITE_RATION_CARD', 'DATE_OF_BIRTH', 'CHIEF_COMPLAINTS', 'Edit'];
        this.patientTableDatasource = new material_1.MatTableDataSource();
        this.isExpansionDetailRow = function (i, row) { return row.hasOwnProperty('detailRow'); };
    }
    PatientsComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.sub = this.route.params.subscribe(function (params) {
            _this.camp_id = +params['campid'];
            console.log(_this.camp_id);
            _this.isLoadingResults = true;
            _this.campsService.getPatients(_this.camp_id).subscribe(function (response) {
                _this.patientTableDatasource.data = response.data;
                _this.isLoadingResults = false;
            });
        });
    };
    PatientsComponent.prototype.applyFilter = function (filterValue) {
        filterValue = filterValue.trim(); // Remove whitespace
        filterValue = filterValue.toLowerCase(); // Datasource defaults to lowercase matches
        this.patientTableDatasource.filter = filterValue;
    };
    PatientsComponent.prototype.onPatientClicked = function (patient) {
        console.log(patient.patient_id);
        this.selectedPatient = patient;
        // this.router.navigateByUrl('/patient/'+patient.patient_id);
    };
    PatientsComponent.prototype.addPatient = function () {
        var dialogRef = this.dialog.open(newpatient_component_1.NewpatientComponent, {
            panelClass: 'contact-form-dialog',
            data: {
                action: 'new'
            }
        });
        dialogRef.afterClosed().subscribe(function (result) {
            console.log("Dialog result: " + result);
        });
    };
    PatientsComponent.prototype.editPatient = function (patient) {
        var dialogRef = this.dialog.open(newpatient_component_1.NewpatientComponent, {
            panelClass: 'contact-form-dialog',
            data: {
                patient: patient,
                action: 'edit'
            }
        });
    };
    __decorate([
        core_1.ViewChild(material_1.MatPaginator)
    ], PatientsComponent.prototype, "paginator");
    __decorate([
        core_1.ViewChild(material_1.MatSort)
    ], PatientsComponent.prototype, "sort");
    PatientsComponent = __decorate([
        core_1.Component({
            selector: 'app-patients',
            templateUrl: './patients.component.html',
            styleUrls: ['./patients.component.scss'],
            animations: [
                animations_1.trigger('detailExpand', [
                    animations_1.state('collapsed', animations_1.style({ height: '0px', minHeight: '0', visibility: 'hidden' })),
                    animations_1.state('expanded', animations_1.style({ height: '*', visibility: 'visible' })),
                    animations_1.transition('expanded <=> collapsed', animations_1.animate('225ms cubic-bezier(0.4, 0.0, 0.2, 1)')),
                ]),
            ]
        })
    ], PatientsComponent);
    return PatientsComponent;
}());
exports.PatientsComponent = PatientsComponent;
