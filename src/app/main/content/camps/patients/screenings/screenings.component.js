"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var core_1 = require('@angular/core');
var animations_1 = require('@fuse/animations');
var material_1 = require('@angular/material');
// import { ScreeningsComponent } from './screenings/screenings.component';
var animations_2 = require('@angular/animations');
var ScreeningsComponent = (function () {
    function ScreeningsComponent(campsService, route, router, dialog) {
        this.campsService = campsService;
        this.route = route;
        this.router = router;
        this.dialog = dialog;
        // camps: any = {
        //     columns: ['patient_id', 'screening_id', 'camp_id', 'SERIAL_NO', 'DISTRICT', 'START_TIME', 'A_S_EXAMINATION', 'A_S_OTHER_CONDITION', 'A_S_REFERRAL_ADVICE',
        // 'CHIEF_COMPLAINTS_DURATION', 'CHIEF_COMPLAINTS_OCULAR_SEQUELAE','CHIEF_COMPLAINTS_SPH','CHIEF_COMPLAINTS_SYSTEMIC_ILLNESS','FINAL_REMARKS','OPTHALMIC_ASSISTANT',
        // 'PINHOLE_LE','PINHOLE_RE','REFRACTION_LEFT_EYE_ADD_LEFT_EYE','REFRACTION_LEFT_EYE_ADD_LEFT_EYE_VA','REFRACTION_LEFT_EYE_AXIS','REFRACTION_LEFT_EYE_CYL',
        // 'REFRACTION_LEFT_EYE_SPH','REFRACTION_LEFT_EYE_VA','REFRACTION_RIGHT_EYE_ADD_RIGHT_EYE','REFRACTION_RIGHT_EYE_ADD_RIGHT_EYE_VA','REFRACTION_RIGHT_EYE_AXIS',
        // 'REFRACTION_RIGHT_EYE_CYL','REFRACTION_RIGHT_EYE_SPH','REFRACTION_RIGHT_EYE_VA','UNAIDED_VA_LE','UNAIDED_VA_RE','create_timestamp','data_entry_time','update_timestamp','Edit'],
        //     rows: [],
        // };
        this.camps = {
            columns: ['patient_id', 'screening_id', 'camp_id', 'SERIAL_NO', 'DISTRICT', 'START_TIME', 'FINAL_REMARKS',
                'create_timestamp', 'data_entry_time', 'update_timestamp', 'Edit'],
            rows: []
        };
        this.dataSourceOriginal = {};
        // private isLoadingResults = false;
        this.patientData = null;
        this.displayedColumns = ['patient_id', 'screening_id', 'camp_id', 'SERIAL_NO', 'DISTRICT', 'START_TIME', 'A_S_EXAMINATION', 'A_S_OTHER_CONDITION', 'A_S_REFERRAL_ADVICE',
            'CHIEF_COMPLAINTS_DURATION', 'CHIEF_COMPLAINTS_OCULAR_SEQUELAE', 'CHIEF_COMPLAINTS_SPH', 'CHIEF_COMPLAINTS_SYSTEMIC_ILLNESS', 'FINAL_REMARKS', 'OPTHALMIC_ASSISTANT',
            'PINHOLE_LE', 'PINHOLE_RE', 'REFRACTION_LEFT_EYE_ADD_LEFT_EYE', 'REFRACTION_LEFT_EYE_ADD_LEFT_EYE_VA', 'REFRACTION_LEFT_EYE_AXIS', 'REFRACTION_LEFT_EYE_CYL',
            'REFRACTION_LEFT_EYE_SPH', 'REFRACTION_LEFT_EYE_VA', 'REFRACTION_RIGHT_EYE_ADD_RIGHT_EYE', 'REFRACTION_RIGHT_EYE_ADD_RIGHT_EYE_VA', 'REFRACTION_RIGHT_EYE_AXIS',
            'REFRACTION_RIGHT_EYE_CYL', 'REFRACTION_RIGHT_EYE_SPH', 'REFRACTION_RIGHT_EYE_VA', 'UNAIDED_VA_LE', 'UNAIDED_VA_RE', 'create_timestamp', 'data_entry_time', 'update_timestamp', 'Edit'];
        this.patientTableDatasource = new material_1.MatTableDataSource();
        this.isExpansionDetailRow = function (i, row) { return row.hasOwnProperty('detailRow'); };
        this.patientScreeningsTableDatasource = new material_1.MatTableDataSource();
        console.log('screenings loaded');
    }
    ScreeningsComponent.prototype.ngOnInit = function () {
        var _this = this;
        console.log('screenings initialized');
        console.log(this.patientId);
        this.campsService.getScreeningsOfPatient(this.patientId).subscribe(function (response) {
            var finalData = [];
            response.data.forEach(function (element) { return finalData.push(element, { detailRow: true, element: element }); });
            _this.patientScreeningsTableDatasource.data = finalData;
            console.log("screenings data", finalData);
            _this.camps.rows = _this.patientScreeningsTableDatasource.data;
            console.log("screenings rows", _this.camps.rows);
        });
    };
    __decorate([
        core_1.ViewChild(material_1.MatPaginator)
    ], ScreeningsComponent.prototype, "paginator");
    __decorate([
        core_1.ViewChild(material_1.MatSort)
    ], ScreeningsComponent.prototype, "sort");
    __decorate([
        core_1.Input()
    ], ScreeningsComponent.prototype, "patientId");
    ScreeningsComponent = __decorate([
        core_1.Component({
            selector: 'app-screenings',
            templateUrl: './screenings.component.html',
            styleUrls: ['./screenings.component.scss'],
            animations: [animations_1.fuseAnimations,
                animations_2.trigger('detailExpand', [
                    animations_2.state('collapsed', animations_2.style({ height: '0px', minHeight: '0', visibility: 'hidden' })),
                    animations_2.state('expanded', animations_2.style({ height: '*', visibility: 'visible' })),
                    animations_2.transition('expanded <=> collapsed', animations_2.animate('225ms cubic-bezier(0.4, 0.0, 0.2, 1)')),
                ]),
            ]
        })
    ], ScreeningsComponent);
    return ScreeningsComponent;
}());
exports.ScreeningsComponent = ScreeningsComponent;
