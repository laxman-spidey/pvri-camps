import { Component, OnInit, Input , ViewChild, Pipe, PipeTransform, Injectable , Inject, AfterViewInit  } from '@angular/core';
import { fuseAnimations } from '@fuse/animations';
import {MatPaginator, MatSort, MatTableDataSource} from '@angular/material';
import {CampsService} from '../../camps.service';
import { ActivatedRoute } from '@angular/router';
import {merge} from 'rxjs/observable/merge';
import {of as observableOf} from 'rxjs/observable/of';
import {catchError} from 'rxjs/operators/catchError';
import {map} from 'rxjs/operators/map';
import {startWith} from 'rxjs/operators/startWith';
import {switchMap} from 'rxjs/operators/switchMap';
import {MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material';
import { NewscreeningComponent } from '../newscreening/newscreening.component';
import { animate, state, style, transition, trigger } from '@angular/animations';
import { Router } from '@angular/router';
import {MatStepperModule} from '@angular/material/stepper';

@Component({
  selector: 'app-screenings',
  templateUrl: './screenings.component.html',
  styleUrls: ['./screenings.component.scss'],
  animations: [ fuseAnimations,
    trigger('detailExpand', [
      state('collapsed', style({ height: '0px', minHeight: '0', visibility: 'hidden' })),
      state('expanded', style({ height: '*', visibility: 'visible' })),
      transition('expanded <=> collapsed', animate('225ms cubic-bezier(0.4, 0.0, 0.2, 1)')),
    ]),
  ],
})
export class ScreeningsComponent implements AfterViewInit {

  private sub: any;
  
  // camps: any = {
  //     columns: ['patient_id', 'screening_id', 'camp_id', 'SERIAL_NO', 'DISTRICT', 'START_TIME', 'A_S_EXAMINATION', 'A_S_OTHER_CONDITION', 'A_S_REFERRAL_ADVICE',
  // 'CHIEF_COMPLAINTS_DURATION', 'CHIEF_COMPLAINTS_OCULAR_SEQUELAE','CHIEF_COMPLAINTS_SPH','CHIEF_COMPLAINTS_SYSTEMIC_ILLNESS','FINAL_REMARKS','OPTHALMIC_ASSISTANT',
  // 'PINHOLE_LE','PINHOLE_RE','REFRACTION_LEFT_EYE_ADD_LEFT_EYE','REFRACTION_LEFT_EYE_ADD_LEFT_EYE_VA','REFRACTION_LEFT_EYE_AXIS','REFRACTION_LEFT_EYE_CYL',
  // 'REFRACTION_LEFT_EYE_SPH','REFRACTION_LEFT_EYE_VA','REFRACTION_RIGHT_EYE_ADD_RIGHT_EYE','REFRACTION_RIGHT_EYE_ADD_RIGHT_EYE_VA','REFRACTION_RIGHT_EYE_AXIS',
  // 'REFRACTION_RIGHT_EYE_CYL','REFRACTION_RIGHT_EYE_SPH','REFRACTION_RIGHT_EYE_VA','UNAIDED_VA_LE','UNAIDED_VA_RE','create_timestamp','data_entry_time','update_timestamp','Edit'],
  //     rows: [],
  // };
  camps: any = {
      columns: ['patient_id', 'screening_id', 'camp_id', 'SERIAL_NO', 'DISTRICT', 'START_TIME', 'FINAL_REMARKS',
  'create_timestamp','data_entry_time','update_timestamp','Edit'],
      rows: [],
  };
  isLoadingResults: boolean;
  dataSourceOriginal: any = {};
  
  camp_id: any; 
  // private isLoadingResults = false;
  patientData: CampsService | null;
  expandedElement: any;
  selectedPatient: any;
  displayedColumns = ['patient_id', 'screening_id', 'camp_id', 'SERIAL_NO', 'DISTRICT', 'START_TIME', 'A_S_EXAMINATION', 'A_S_OTHER_CONDITION', 'A_S_REFERRAL_ADVICE',
  'CHIEF_COMPLAINTS_DURATION', 'CHIEF_COMPLAINTS_OCULAR_SEQUELAE','CHIEF_COMPLAINTS_SPH','CHIEF_COMPLAINTS_SYSTEMIC_ILLNESS','FINAL_REMARKS','OPTHALMIC_ASSISTANT',
  'PINHOLE_LE','PINHOLE_RE','REFRACTION_LEFT_EYE_ADD_LEFT_EYE','REFRACTION_LEFT_EYE_ADD_LEFT_EYE_VA','REFRACTION_LEFT_EYE_AXIS','REFRACTION_LEFT_EYE_CYL',
  'REFRACTION_LEFT_EYE_SPH','REFRACTION_LEFT_EYE_VA','REFRACTION_RIGHT_EYE_ADD_RIGHT_EYE','REFRACTION_RIGHT_EYE_ADD_RIGHT_EYE_VA','REFRACTION_RIGHT_EYE_AXIS',
  'REFRACTION_RIGHT_EYE_CYL','REFRACTION_RIGHT_EYE_SPH','REFRACTION_RIGHT_EYE_VA','UNAIDED_VA_LE','UNAIDED_VA_RE','create_timestamp','data_entry_time','update_timestamp','Edit'];
  patientTableDatasource = new MatTableDataSource();
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;
  isExpansionDetailRow = (i: number, row: Object) => row.hasOwnProperty('detailRow');
  
  patientScreeningsTableDatasource = new MatTableDataSource();
  
  @Input() patientId;
  constructor(
                private campsService: CampsService,
                private route: ActivatedRoute, 
              private router: Router,
              public dialog: MatDialog,
              ) {
    console.log('screenings loaded');
    
   }

  ngOnInit() {
    // console.log('screenings initialized');
    // console.log(this.patientId);
    // this.campsService.getScreeningsOfPatient(this.patientId).subscribe((response) => {
    //   const finalData = [];
    //   response.data.forEach(element => finalData.push(element, { detailRow: true, element }));
    //   this.patientScreeningsTableDatasource.data = finalData;
    //   console.log("screenings data",finalData);
    //   this.camps.rows = this.patientScreeningsTableDatasource.data;
    //   console.log("screenings rows",this.camps.rows);
    // });
  }
  
  
ngAfterViewInit(){
  console.log('screenings initialized');
    console.log(this.patientId);
    this.campsService.getScreeningsOfPatient(this.patientId).subscribe((response) => {
      const finalData = [];
      response.data.forEach(element => finalData.push(element, { detailRow: true, element }));
      this.patientScreeningsTableDatasource.data = finalData;
      console.log("screenings data",finalData);
      this.camps.rows = this.patientScreeningsTableDatasource.data;
      console.log("screenings rows",this.camps.rows);
    });
}

addScreenings()
  {
    const dialogRef = this.dialog.open(NewscreeningComponent  , {
      panelClass: 'contact-form-dialog',
      data : {
        action: 'new'
      }
    });
    
    dialogRef.afterClosed().subscribe(result => {
      console.log(`Dialog result: ${result}`);
    });
  }
  
}
