import { Component, OnInit } from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {MatStepperModule} from '@angular/material/stepper';
import {HttpClient} from '@angular/common/http';
import {NetworkService, API} from 'app/network.service';
import { CampsService } from '../../camps.service';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/observable/forkJoin';
import { Screening } from './newscreening.model';

@Component({
  selector: 'app-newscreening',
  templateUrl: './newscreening.component.html',
  styleUrls: ['./newscreening.component.scss']
})
export class NewscreeningComponent implements OnInit {
  
  isLinear = true;
  firstFormGroup: FormGroup;
  secondFormGroup: FormGroup;
  thirdFormGroup: FormGroup;
  fourthFormGroup: FormGroup;
  fifthFormGroup: FormGroup;

  districts = [];selectedDistrict = '';
  
  screening={};
  
  constructor(private formBuilder: FormBuilder,private http: HttpClient, 
        private networkService: NetworkService,
        private campsService: CampsService) { 
          
          // this.isLoadingResults = true;
        Observable.forkJoin(
        this.networkService.get(API.GET_DISTRICTS)
        ).subscribe(response => {
            
            this.districts = response[0].data;
            console.log(JSON.stringify(this.districts));
            // this.isLoadingResults = false;
        });
        }

  ngOnInit() {
    this.firstFormGroup = this.formBuilder.group({
      patient_id: ['', Validators.required],
      screeningId: ['', Validators.required],
      campId: ['', Validators.required],
      district: [this.selectedDistrict, Validators.required],
      serialNo: ['', Validators.required],
      finalRemarks: ['', Validators.required]
    });
    this.secondFormGroup = this.formBuilder.group({
      A_S_EXAMINATION: [''],
      A_S_OTHER_CONDITION: [''],
      A_S_REFERRAL_ADVICE: ['']
    });
    this.thirdFormGroup = this.formBuilder.group({
      CHIEF_COMPLAINTS_DURATION: [''],
      CHIEF_COMPLAINTS_OCULAR_SEQUELAE: [''],
      CHIEF_COMPLAINTS_SPH: [''],
      CHIEF_COMPLAINTS_SYSTEMIC_ILLNESS: [''],
      OPTHALMIC_ASSISTANT: ['']
    });
    this.fifthFormGroup = this.formBuilder.group({
      START_TIME: [''],
      create_timestamp: [''],
      data_entry_time: [''],
      update_timestamp: ['']
    });
    this.fourthFormGroup = this.formBuilder.group({
      REFRACTION_LEFT_EYE_ADD_LEFT_EYE: [''],
      REFRACTION_LEFT_EYE_ADD_LEFT_EYE_VA: [''],
      REFRACTION_LEFT_EYE_AXIS: [''],
      REFRACTION_LEFT_EYE_CYL: [''],
      REFRACTION_LEFT_EYE_SPH: [''],
      REFRACTION_LEFT_EYE_VA: [''],
      REFRACTION_RIGHT_EYE_ADD_RIGHT_EYE: [''],
      REFRACTION_RIGHT_EYE_ADD_RIGHT_EYE_VA: [''],
      REFRACTION_RIGHT_EYE_AXIS: [''],
      REFRACTION_RIGHT_EYE_CYL: [''],
      REFRACTION_RIGHT_EYE_SPH: [''],
      REFRACTION_RIGHT_EYE_VA: [''],
      PINHOLE_LE: [''],
      PINHOLE_RE: [''],
      UNAIDED_VA_LE: [''],
      UNAIDED_VA_RE: ['']
    });

  }
  
  createAScreening() {
        const screening = new Screening(
                                '0',
                                // this.firstFormGroup.get('patient_id').value,
                                this.firstFormGroup.get('screeningId').value,
                                this.firstFormGroup.get('campId').value,
                                this.firstFormGroup.get('district').value,
                                this.firstFormGroup.get('serialNo').value,
                                this.firstFormGroup.get('finalRemarks').value,
                                this.secondFormGroup.get('A_S_EXAMINATION').value,
                                this.secondFormGroup.get('A_S_OTHER_CONDITION').value,
                                this.secondFormGroup.get('A_S_REFERRAL_ADVICE').value,
                                this.thirdFormGroup.get('CHIEF_COMPLAINTS_DURATION').value,
                                this.thirdFormGroup.get('CHIEF_COMPLAINTS_OCULAR_SEQUELAE').value,
                                this.thirdFormGroup.get('CHIEF_COMPLAINTS_SPH').value,
                                this.thirdFormGroup.get('CHIEF_COMPLAINTS_SYSTEMIC_ILLNESS').value,
                                this.thirdFormGroup.get('OPTHALMIC_ASSISTANT').value,
                                this.fourthFormGroup.get('REFRACTION_LEFT_EYE_ADD_LEFT_EYE').value,
                                this.fourthFormGroup.get('REFRACTION_LEFT_EYE_ADD_LEFT_EYE_VA').value,
                                this.fourthFormGroup.get('REFRACTION_LEFT_EYE_AXIS').value,
                                this.fourthFormGroup.get('REFRACTION_LEFT_EYE_CYL').value,
                                this.fourthFormGroup.get('REFRACTION_LEFT_EYE_SPH').value,
                                this.fourthFormGroup.get('REFRACTION_LEFT_EYE_VA').value,
                                this.fourthFormGroup.get('REFRACTION_RIGHT_EYE_ADD_RIGHT_EYE').value,
                                this.fourthFormGroup.get('REFRACTION_RIGHT_EYE_ADD_RIGHT_EYE_VA').value,
                                this.fourthFormGroup.get('REFRACTION_RIGHT_EYE_AXIS').value,
                                this.fourthFormGroup.get('REFRACTION_RIGHT_EYE_CYL').value,
                                this.fourthFormGroup.get('REFRACTION_RIGHT_EYE_SPH').value,
                                this.fourthFormGroup.get('REFRACTION_RIGHT_EYE_VA').value,
                                this.fourthFormGroup.get('PINHOLE_LE').value,
                                this.fourthFormGroup.get('PINHOLE_RE').value,
                                this.fourthFormGroup.get('UNAIDED_VA_LE').value,
                                this.fourthFormGroup.get('UNAIDED_VA_RE').value,
                                this.fifthFormGroup.get('START_TIME').value,
                                this.fifthFormGroup.get('create_timestamp').value,
                                this.fifthFormGroup.get('data_entry_time').value,
                                this.fifthFormGroup.get('update_timestamp').value,
                            );
        // if (this.action === 'edit') {  
        //   console.log("post patient"+patient);
        //     this.campsService.updatePatient(patient).subscribe((response) => {        
        //     // todo
        //   });
        // }
        // else {
            // patient.ration_card_img = undefined;
            // patient.patient_img = undefined;
            console.log("post screening"+JSON.stringify(screening));
            // console.log("post patient"+patient.patient_img);
            this.campsService.createAScreening(screening).subscribe((response) => {        
                // todo
            });
        // }
    }

}
