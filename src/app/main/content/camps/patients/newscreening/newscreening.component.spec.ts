import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NewscreeningComponent } from './newscreening.component';

describe('NewscreeningComponent', () => {
  let component: NewscreeningComponent;
  let fixture: ComponentFixture<NewscreeningComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NewscreeningComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NewscreeningComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
