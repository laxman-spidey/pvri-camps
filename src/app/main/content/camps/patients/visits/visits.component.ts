import { Component, OnInit } from '@angular/core';
import { fuseAnimations } from '@fuse/animations';

@Component({
  selector: 'app-visits',
  templateUrl: './visits.component.html',
  styleUrls: ['./visits.component.scss'],
  animations: [fuseAnimations]
})
export class VisitsComponent implements OnInit {

  constructor() { 
    console.log('visits loaded');
  }

  ngOnInit() {
    console.log('visits initilized');
  }

}
