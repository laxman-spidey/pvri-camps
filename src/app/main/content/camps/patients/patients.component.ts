import {Component, OnInit, ViewChild, Pipe, PipeTransform, Injectable , Inject } from '@angular/core';
import {MatPaginator, MatSort, MatTableDataSource} from '@angular/material';
import {merge} from 'rxjs/observable/merge';
import {of as observableOf} from 'rxjs/observable/of';
import {catchError} from 'rxjs/operators/catchError';
import {map} from 'rxjs/operators/map';
import {startWith} from 'rxjs/operators/startWith';
import {switchMap} from 'rxjs/operators/switchMap';
import {CampsService} from '../camps.service';
import { NewcampComponent } from '../newcamp/newcamp.component';
import {MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material';
import { ActivatedRoute } from '@angular/router';
import { Router } from '@angular/router';
import { NewpatientComponent } from './newpatient/newpatient.component';
import { fuseAnimations } from '@fuse/animations';
import { animate, state, style, transition, trigger } from '@angular/animations';

@Component({
  selector: 'app-patients',
  templateUrl: './patients.component.html',
  styleUrls: ['./patients.component.scss'],
  animations: [ fuseAnimations,
    trigger('detailExpand', [
      state('collapsed', style({ height: '0px', minHeight: '0', visibility: 'hidden' })),
      state('expanded', style({ height: '*', visibility: 'visible' })),
      transition('expanded <=> collapsed', animate('225ms cubic-bezier(0.4, 0.0, 0.2, 1)')),
    ]),
  ],
})
export class PatientsComponent implements OnInit {
  camp_id: any;  
  private sub: any;
  private isLoadingResults = false;
  patientData: CampsService | null;
  expandedElement: any;
  selectedPatient: any;
  // tslint:disable-next-line:max-line-length
  displayedColumns = ['patient_id', 'patient_name', 'HEAD_OF_FAMILY_NAME', 'AADHAR', 'AGE', 'CONTACT_NUMBER', 'GENDER', 'BELOW_POVERTY_LINE', 'WHITE_RATION_CARD', 'DATE_OF_BIRTH', 'CHIEF_COMPLAINTS', 'Edit'];
  patientTableDatasource = new MatTableDataSource();
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;
  isExpansionDetailRow = (i: number, row: Object) => row.hasOwnProperty('detailRow');
  constructor(private route: ActivatedRoute, 
              private router: Router, 
              private campsService: CampsService,
              public dialog: MatDialog,
              
              ) {}
  ngOnInit() {
    
    this.sub = this.route.params.subscribe(params => {
      this.camp_id = +params['campid'];
      console.log(this.camp_id);
      this.isLoadingResults = true;
      this.campsService.getPatients(this.camp_id).subscribe((response) => {
        const finalData = [];
        response.data.forEach(element => finalData.push(element, { detailRow: true, element }));
        this.patientTableDatasource.data = finalData;
        console.log('patient final data: ', this.patientTableDatasource);
        this.isLoadingResults = false;
      });
    });
  }
  applyFilter(filterValue: string) {
    filterValue = filterValue.trim(); // Remove whitespace
    filterValue = filterValue.toLowerCase(); // Datasource defaults to lowercase matches
    this.patientTableDatasource.filter = filterValue;
  }
  
  onPatientClicked(patient: any)
  {
    console.log(patient.patient_id);
    this.selectedPatient = patient;
    // this.router.navigateByUrl('/patient/'+patient.patient_id);
  }
   
  addPatient()
  {
    const dialogRef = this.dialog.open(NewpatientComponent  , {
      panelClass: 'contact-form-dialog',
      data : {
        action: 'new'
      }
    });
    
    dialogRef.afterClosed().subscribe(result => {
      console.log(`Dialog result: ${result}`);
    });
  }
  editPatient(patient){
    const dialogRef = this.dialog.open(NewpatientComponent, {
            panelClass: 'contact-form-dialog',
            data      : {
                patient: patient,
                action : 'edit'
            }
        });
  }
}
