"use strict";
var Patient = (function () {
    function Patient(patient_id, patientName, whiteRationCard, familyHeadName, contactNumber, age, dob, gender, aadhar, selRationCardImage, selPatientImage) {
        this.patient_id = patient_id;
        this.patient_name = patientName;
        this.WHITE_RATION_CARD = whiteRationCard ? 'yes' : 'no';
        this.HEAD_OF_FAMILY_NAME = familyHeadName;
        this.CONTACT_NUMBER = contactNumber;
        this.AGE = age;
        this.DATE_OF_BIRTH = dob;
        this.GENDER = gender;
        this.AADHAR = aadhar;
        this.ration_card_img = selRationCardImage;
        this.patient_img = selPatientImage;
    }
    return Patient;
}());
exports.Patient = Patient;
