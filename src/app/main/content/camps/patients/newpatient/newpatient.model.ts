import { FuseUtils } from '@fuse/utils';

export class Patient
{
    patient_id: string;
    patient_name: string;
    WHITE_RATION_CARD: string;
    HEAD_OF_FAMILY_NAME: string;
    CONTACT_NUMBER: string;
    AGE: string;
    DATE_OF_BIRTH: string;
    AADHAR: string;
    GENDER: string;
    ration_card_img: string;
    patient_img: string;
    
    constructor(patient_id, 
                patientName, 
                whiteRationCard, 
                familyHeadName, 
                contactNumber,
                age,
                dob,
                gender,
                aadhar,
                selRationCardImage,
                selPatientImage) {
        this.patient_id = patient_id;
        this.patient_name = patientName;
        this.WHITE_RATION_CARD = whiteRationCard ? 'yes' : 'no';
        this.HEAD_OF_FAMILY_NAME = familyHeadName;
        this.CONTACT_NUMBER = contactNumber;
        this.AGE = age;
        this.DATE_OF_BIRTH = dob;
        this.GENDER = gender;
        this.AADHAR = aadhar;
        this.ration_card_img = selRationCardImage;
        this.patient_img = selPatientImage;

    }
}
