"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
var core_1 = require('@angular/core');
var material_1 = require('@angular/material');
var forms_1 = require('@angular/forms');
var animations_1 = require('@fuse/animations');
var newpatient_model_1 = require('./newpatient.model');
var NewpatientComponent = (function () {
    function NewpatientComponent(dialogRef, data, formBuilder, element, changeDetectorRef, http, networkService, campsService) {
        this.dialogRef = dialogRef;
        this.data = data;
        this.formBuilder = formBuilder;
        this.element = element;
        this.http = http;
        this.networkService = networkService;
        this.campsService = campsService;
        this.patient = {
            patient_id: '',
            patient_name: '',
            HEAD_OF_FAMILY_NAME: '',
            GENDER: '',
            BELOW_POVERTY_LINE: '',
            DATE_OF_BIRTH: '',
            CHIEF_COMPLAINTS: '',
            AADHAR: '',
            AGE: '',
            CONTACT_NUMBER: '',
            WHITE_RATION_CARD: '',
            patient_img: ''
        };
        this.action = 'new';
        this.imageURI = "http://placehold.it/200x200";
        this.genders = [{ gender_id: 'male', gender_name: 'Male' }, { gender_id: 'female', gender_name: 'Female' }];
        this.file_srcs = [];
        this.selRationCardImage = '';
        this.selRationCardImageName = '';
        this.selPatientImage = '';
        // The next two lines are just to show the resize debug
        // they can be removed
        this.debug_size_before = [];
        this.debug_size_after = [];
        this.action = data.action;
        console.log('action', this.action);
        if (this.action === 'edit') {
            this.dialogTitle = 'Edit Contact';
            console.log('patient data: ', data.patient);
            this.patient = data.patient;
        }
        else {
            this.dialogTitle = 'New Patient';
            this.patient = this.patient;
        }
        this.newPatientForm = this.createPatientForm();
    }
    NewpatientComponent.prototype.createPatientForm = function () {
        return this.formBuilder.group({
            patientName: [this.patient.patient_name, forms_1.Validators.required],
            whiteRationCard: [this.patient.WHITE_RATION_CARD, forms_1.Validators.required],
            familyHeadName: [this.patient.HEAD_OF_FAMILY_NAME, forms_1.Validators.required],
            age: [this.patient.AGE, forms_1.Validators.required],
            gender: [this.patient.GENDER, forms_1.Validators.required],
            contactNumber: [this.patient.CONTACT_NUMBER, [forms_1.Validators.required, forms_1.Validators.pattern('[0-9]*'), forms_1.Validators.minLength(10), forms_1.Validators.maxLength(10)]],
            aadhar: [this.patient.AADHAR, [forms_1.Validators.required, forms_1.Validators.pattern('[0-9]*'), forms_1.Validators.minLength(12), forms_1.Validators.maxLength(12)]],
            dob: [(this.patient.DATE_OF_BIRTH == null) ? '' : new Date(this.patient.DATE_OF_BIRTH), forms_1.Validators.required],
            patient_img: [(this.patient.patient_img == '') ? '' : new Date(this.patient.patient_img), forms_1.Validators.required]
        });
    };
    NewpatientComponent.prototype.fileSelected = function (event) {
        var _this = this;
        if (event.target.files && event.target.files[0]) {
            // alert("preview");
            var reader = new FileReader();
            reader.onload = function (event) {
                _this.imageURI = event.target.result;
                _this.selPatientImage = event.target.result;
            };
            reader.readAsDataURL(event.target.files[0]);
            var fileList = event.target.files;
            var file = fileList[0];
        }
        if (event.target.files && event.target.files[0]) {
            // alert("file");
            var reader = new FileReader();
            var fileList = event.target.files;
            var file = fileList[0];
            reader.readAsBinaryString(file);
            console.log(file.name);
            // this.imageName = file.name;
            reader.onload = function (event) {
                // this.imageURI = event.target.result;
                // this.imageURIdata = this.imageURI;
                // this.imageURIdata    = btoa(reader.result);
            };
            reader.readAsDataURL(event.target.files[0]);
        }
    };
    NewpatientComponent.prototype.readAsBase64 = function (event) {
        var _this = this;
        var reader = new FileReader();
        var fileList = event.target.files;
        var file = fileList[0];
        reader.readAsBinaryString(file);
        console.log(file.name);
        reader.onload = function (readEvent) {
            _this.profilePicture = btoa(reader.result);
            console.log(_this.profilePicture);
        };
        reader.readAsDataURL(event.target.files[0]);
    };
    // This is called when the user selects new files from the upload button
    NewpatientComponent.prototype.fileChange = function (input) {
        // this.rationImg = this.readFiles(input.files);
        this.readFiles(input.files);
    };
    NewpatientComponent.prototype.readFile = function (file, reader, callback) {
        // Set a callback funtion to fire after the file is fully loaded
        reader.onload = function () {
            // callback with the results
            callback(reader.result);
        };
        // Read the file
        // reader.cd(file);
        reader.readAsDataURL(file);
    };
    NewpatientComponent.prototype.readFiles = function (files, index) {
        var _this = this;
        if (index === void 0) { index = 0; }
        // Create the file reader
        var reader = new FileReader();
        // If there is a file
        if (index in files) {
            // Start reading this file
            this.readFile(files[index], reader, function (result) {
                // Create an img element and add the image file data to it
                var img = document.createElement('img');
                img.src = result;
                _this.selRationCardImage = result;
                console.log(files[index].name);
                _this.selRationCardImageName = files[index].name;
                // Send this img to the resize function (and wait for callback)
                _this.resize(img, 250, 250, function (resized_jpeg, before, after) {
                    // For debugging (size in bytes before and after)
                    _this.debug_size_before.push(before);
                    _this.debug_size_after.push(after);
                    // Add the resized jpeg img source to a list for preview
                    // This is also the file you want to upload. (either as a
                    // base64 string or img.src = resized_jpeg if you prefer a file). 
                    _this.file_srcs.push(resized_jpeg);
                    // Read the next file;
                    _this.readFiles(files, index + 1);
                });
            });
        }
        else {
        }
    };
    NewpatientComponent.prototype.resize = function (img, MAX_WIDTH, MAX_HEIGHT, callback) {
        // This will wait until the img is loaded before calling this function
        return img.onload = function () {
            console.log('img loaded');
            // Get the images current width and height
            var width = img.width;
            var height = img.height;
            // Set the WxH to fit the Max values (but maintain proportions)
            if (width > height) {
                if (width > MAX_WIDTH) {
                    height *= MAX_WIDTH / width;
                    width = MAX_WIDTH;
                }
            }
            else {
                if (height > MAX_HEIGHT) {
                    width *= MAX_HEIGHT / height;
                    height = MAX_HEIGHT;
                }
            }
            // create a canvas object
            var canvas = document.createElement('canvas');
            // Set the canvas to the new calculated dimensions
            canvas.width = width;
            canvas.height = height;
            var ctx = canvas.getContext('2d');
            ctx.drawImage(img, 0, 0, width, height);
            // Get this encoded as a jpeg
            // IMPORTANT: 'jpeg' NOT 'jpg'
            var dataUrl = canvas.toDataURL('image/jpeg');
            // callback with the results
            callback(dataUrl, img.src.length, dataUrl.length);
        };
    };
    NewpatientComponent.prototype.ngOnInit = function () {
    };
    NewpatientComponent.prototype.createAPatient = function () {
        var patient = new newpatient_model_1.Patient('0', this.newPatientForm.get('patientName').value, this.newPatientForm.get('whiteRationCard').value, this.newPatientForm.get('familyHeadName').value, this.newPatientForm.get('contactNumber').value, this.newPatientForm.get('age').value, this.newPatientForm.get('dob').value, this.newPatientForm.get('gender').value, this.newPatientForm.get('aadhar').value, this.selRationCardImage, this.selPatientImage);
        if (this.action === 'edit') {
            console.log("post patient" + patient);
            this.campsService.updatePatient(patient).subscribe(function (response) {
                // todo
            });
        }
        else {
            patient.ration_card_img = undefined;
            patient.patient_img = undefined;
            console.log("post patient" + JSON.stringify(patient));
            console.log("post patient" + patient.patient_img);
            this.campsService.createAPatient(patient, this.selRationCardImage).subscribe(function (response) {
                // todo
            });
        }
    };
    NewpatientComponent = __decorate([
        core_1.Component({
            selector: 'app-newpatient',
            templateUrl: './newpatient.component.html',
            styleUrls: ['./newpatient.component.scss'],
            encapsulation: core_1.ViewEncapsulation.None,
            animations: animations_1.fuseAnimations
        }),
        __param(1, core_1.Inject(material_1.MAT_DIALOG_DATA))
    ], NewpatientComponent);
    return NewpatientComponent;
}());
exports.NewpatientComponent = NewpatientComponent;
