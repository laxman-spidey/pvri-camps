import { Component, OnInit, ViewEncapsulation ,ElementRef,ChangeDetectorRef,Inject} from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material';
import { FormBuilder, FormGroup, FormControl, Validators ,ValidatorFn} from '@angular/forms';
import { CalendarEvent } from 'angular-calendar';
import {HttpClient} from '@angular/common/http';
import { fuseAnimations } from '@fuse/animations';
import { Patient } from './newpatient.model';
import { CampsService } from '../../camps.service';
import {NetworkService, API} from 'app/network.service';

@Component({
  selector: 'app-newpatient',
  templateUrl: './newpatient.component.html',
  styleUrls: ['./newpatient.component.scss'],
  encapsulation: ViewEncapsulation.None,
  animations   : fuseAnimations
})
export class NewpatientComponent implements OnInit {
  
  newPatientForm: FormGroup;dialogTitle: string;
  
  patient = {
    patient_id : '',
    patient_name : '',
    HEAD_OF_FAMILY_NAME : '',
    GENDER : '',
    BELOW_POVERTY_LINE : '',
    DATE_OF_BIRTH : '',
    CHIEF_COMPLAINTS : '',
    AADHAR : '',
    AGE : '',
    CONTACT_NUMBER : '',
    WHITE_RATION_CARD : '',
    patient_img : ''
  };
  profilePicture: string;
  action = 'new';imageURI="http://placehold.it/200x200";
  genders=[{gender_id:'male',gender_name:'Male'},{gender_id:'female',gender_name:'Female'}];
  public file_srcs: string[] = [];selRationCardImage='';rationImg:any;patientImg:any;selRationCardImageName='';selPatientImage='';

  constructor(
    public dialogRef: MatDialogRef<NewpatientComponent, any>,@Inject(MAT_DIALOG_DATA) private data: any,
    private formBuilder: FormBuilder,private element: ElementRef, changeDetectorRef: ChangeDetectorRef,
    private http: HttpClient, 
        private networkService: NetworkService,private campsService: CampsService,
  ) 
  {
        this.action = data.action;
        console.log('action', this.action);

        if ( this.action === 'edit' )
        {
            this.dialogTitle = 'Edit Contact';
            console.log('patient data: ', data.patient);
            this.patient = data.patient;
        }
        else
        {
            this.dialogTitle = 'New Patient';
            this.patient = this.patient;
            
        }

        this.newPatientForm = this.createPatientForm();
  }
  
  createPatientForm()
    {
      
        return this.formBuilder.group({
            patientName: [this.patient.patient_name, Validators.required],
      whiteRationCard: [this.patient.WHITE_RATION_CARD, Validators.required],
      familyHeadName: [this.patient.HEAD_OF_FAMILY_NAME, Validators.required],
      age: [this.patient.AGE, Validators.required],
      gender: [this.patient.GENDER, Validators.required],
      contactNumber: [this.patient.CONTACT_NUMBER, [Validators.required,Validators.pattern('[0-9]*'), Validators.minLength(10),Validators.maxLength(10)]],
      aadhar: [this.patient.AADHAR, [Validators.required,Validators.pattern('[0-9]*'), Validators.minLength(12),Validators.maxLength(12)]],
      dob: [(this.patient.DATE_OF_BIRTH == null) ? '' : new Date(this.patient.DATE_OF_BIRTH), Validators.required],
      patient_img : [(this.patient.patient_img == '') ? '' : new Date(this.patient.patient_img), Validators.required],
      
        });
    }
    
    fileSelected(event) {
    if (event.target.files && event.target.files[0]) {
      // alert("preview");
      let reader = new FileReader();
      reader.onload = (event: any) => {
        this.imageURI = event.target.result;
         this.selPatientImage = event.target.result;

      }
        reader.readAsDataURL(event.target.files[0]);
        let fileList: FileList = event.target.files;
        let file: File = fileList[0];
    }
    if (event.target.files && event.target.files[0]) {
      // alert("file");
      let reader = new FileReader();
      let fileList: FileList = event.target.files;
      let file: File = fileList[0];
       reader.readAsBinaryString(file);
       console.log(file.name);
      // this.imageName = file.name;
      reader.onload = (event: any) => {
        // this.imageURI = event.target.result;
        // this.imageURIdata = this.imageURI;
        // this.imageURIdata    = btoa(reader.result);
      }
      reader.readAsDataURL(event.target.files[0]);
    }


  }
    

  readAsBase64(event)
  {
    const reader = new FileReader();
    const fileList: FileList = event.target.files;
    const file: File = fileList[0];
    reader.readAsBinaryString(file);
    console.log(file.name);
    reader.onload = (readEvent: any) => {
      this.profilePicture = btoa(reader.result);
      console.log(this.profilePicture);
    };
    reader.readAsDataURL(event.target.files[0]);
  }
  // The next two lines are just to show the resize debug
  // they can be removed
  public debug_size_before: string[] = [];
  public debug_size_after: string[] = [];
  
  // This is called when the user selects new files from the upload button
  fileChange(input){
    // this.rationImg = this.readFiles(input.files);
     this.readFiles(input.files);
  }
  
  readFile(file, reader, callback){
    // Set a callback funtion to fire after the file is fully loaded
    reader.onload = () => {
      // callback with the results
      callback(reader.result);
    };
    
    // Read the file
    // reader.cd(file);
    reader.readAsDataURL(file);
  }
  
    readFiles(files, index=0){
      // Create the file reader
      let reader = new FileReader();
      
      // If there is a file
      if (index in files){
        // Start reading this file
        this.readFile(files[index], reader, (result) =>{
          // Create an img element and add the image file data to it
          var img = document.createElement('img');
          img.src = result;
          this.selRationCardImage=result;
          console.log(files[index].name);
          this.selRationCardImageName=files[index].name;
          // Send this img to the resize function (and wait for callback)
          this.resize(img, 250, 250, (resized_jpeg, before, after)=>{
            // For debugging (size in bytes before and after)
            this.debug_size_before.push(before);
            this.debug_size_after.push(after);
  
            // Add the resized jpeg img source to a list for preview
            // This is also the file you want to upload. (either as a
            // base64 string or img.src = resized_jpeg if you prefer a file). 
            this.file_srcs.push(resized_jpeg);
            
            // Read the next file;
            this.readFiles(files, index+1);
          });
        });
      }else{
        // When all files are done This forces a change detection
        // this.changeDetectorRef.detectChanges();
      }
    }

  
  resize(img, MAX_WIDTH:number, MAX_HEIGHT:number, callback){
    // This will wait until the img is loaded before calling this function
    return img.onload = () => {
      console.log('img loaded');
      // Get the images current width and height
      var width = img.width;
      var height = img.height;
      
      // Set the WxH to fit the Max values (but maintain proportions)
      if (width > height) {
          if (width > MAX_WIDTH) {
              height *= MAX_WIDTH / width;
              width = MAX_WIDTH;
          }
      } else {
          if (height > MAX_HEIGHT) {
              width *= MAX_HEIGHT / height;
              height = MAX_HEIGHT;
          }
      }
      
      // create a canvas object
      var canvas = document.createElement('canvas');
    
      // Set the canvas to the new calculated dimensions
      canvas.width = width;
      canvas.height = height;
      var ctx = canvas.getContext('2d');  

      ctx.drawImage(img, 0, 0,  width, height); 
      
      // Get this encoded as a jpeg
      // IMPORTANT: 'jpeg' NOT 'jpg'
      var dataUrl = canvas.toDataURL('image/jpeg');
      
      // callback with the results
      callback(dataUrl, img.src.length, dataUrl.length);
    };
  }
  
  ngOnInit() {
  }

  createAPatient() {
        const patient = new Patient(
                                '0',
                                this.newPatientForm.get('patientName').value,
                                this.newPatientForm.get('whiteRationCard').value,
                                this.newPatientForm.get('familyHeadName').value,
                                this.newPatientForm.get('contactNumber').value,
                                this.newPatientForm.get('age').value,
                                this.newPatientForm.get('dob').value,
                                this.newPatientForm.get('gender').value,
                                this.newPatientForm.get('aadhar').value,
                                this.selRationCardImage,
                                this.selPatientImage
                            );
        if (this.action === 'edit') {  
          console.log("post patient"+patient);
            this.campsService.updatePatient(patient).subscribe((response) => {        
            // todo
           });
        }
        else {
            patient.ration_card_img = undefined;
            patient.patient_img = undefined;
            console.log("post patient"+JSON.stringify(patient));
            console.log("post patient"+patient.patient_img);
            this.campsService.createAPatient(patient,this.selRationCardImage).subscribe((response) => {        
                // todo
            });
        }
    }


}
