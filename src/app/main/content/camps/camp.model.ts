import { Screening } from './patients/newscreening/newscreening.model';
export class Camp
{
    camp_id: string;
    camp_place: string;
    camp_date: string;
    location_name: string;
    location_id: string;
    project_id: string;
    state_code: string;
    state_name: string;
    district_code: string;
    district_name: string;
    mandal_code: string;
    mandal_id: string;
    mandal_name: string;
    coordinator_name: string;
    camp_coordinator_id: string;
    camp_opthalmologist_id: string;
    opthalmologist_name: string;
    latitude: string;
    longitude: string;
    screenings: string;

    getEditableCamp(): Camp {
        const camp = new Camp(this);
        camp.location_id = undefined;
        camp.mandal_code = undefined;
        camp.mandal_name = undefined;
        camp.location_name = undefined;
        camp.state_code = undefined;
        camp.state_name = undefined;
        camp.district_code = undefined;
        camp.district_name = undefined;
        camp.district_name = undefined;
        camp.coordinator_name = undefined;
        return camp;
    }

    setDate(dateString: string) {
        const todayTime = new Date(dateString);
        const month = todayTime .getMonth() + 1;
        const day = todayTime .getDate();
        const year = todayTime .getFullYear();
        this.camp_date = year + '/' + month + '/' + day;
    }
    
    constructor(object = null)
    {
        if (object != null) 
        {
            this.camp_id = object.camp_id || undefined;
            this.camp_place = object.camp_place || undefined;
            this.camp_date = object.camp_date || undefined;
            this.location_name = object.location_name || undefined;
            this.location_id = object.location_id || undefined;
            this.project_id = object.project_id || undefined;
            this.state_code = object.state_code || undefined;
            this.state_name = object.state_name || undefined;
            this.district_code = object.district_code || undefined;
            this.district_name = object.district_name || undefined;
            this.mandal_code = object.mandal_code || undefined;
            this.mandal_id = object.mandal_id || undefined;
            this.mandal_name = object.mandal_name || undefined;
            this.coordinator_name = object.coordinator_name || undefined;
            this.camp_coordinator_id = object.camp_coordinator_id || undefined;
            this.camp_opthalmologist_id = object.camp_opthalmologist_id || undefined;
            this.opthalmologist_name = object.opthalmologist_name || undefined;
            this.latitude = object.latitude || undefined;
            this.longitude = object.longitude || undefined;
            this.screenings = object.Screenings || undefined;
        }
            // this.name = contact.name || undefined;
            
    }

    
}

