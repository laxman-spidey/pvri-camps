"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var core_1 = require('@angular/core');
var material_1 = require('@angular/material');
var newcamp_component_1 = require('./newcamp/newcamp.component');
var animations_1 = require('@fuse/animations');
var newcamp_model_1 = require('./newcamp/newcamp.model');
var CampsComponent = (function () {
    function CampsComponent(campsService, dialog, router) {
        this.campsService = campsService;
        this.dialog = dialog;
        this.router = router;
        this.camps = {
            columns: ['camp_id', 'Camp', 'Camp held on', 'State', 'District', 'Mandal', 'Camp Coordinator', 'Opthalmologist', 'screenings', 'Edit'],
            rows: []
        };
        this.dataSourceOriginal = {};
        this.campsDataSource = new material_1.MatTableDataSource();
    }
    CampsComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.isLoadingResults = true;
        this.campsService.getCamps().subscribe(function (data) {
            _this.isLoadingResults = false;
            console.log(data.data);
            // data = JSON.stringify(data);  // for mock data
            _this.campsDataSource.data = data.data;
            _this.dataSourceOriginal.data = data.data;
            _this.camps.rows = _this.campsDataSource.data;
            console.log(_this.campsDataSource.data);
            console.log(_this.isLoadingResults);
        });
        // this.sort.sortChange.subscribe(() => this.paginator.pageIndex = 0);
        //   merge(this.sort.sortChange, this.paginator.page)
        //     .pipe(
        //       startWith({}),
        //       switchMap(() => {
        //         this.isLoadingResults = true;
        //         // return this.exampleDatabase!.getRepoIssues(
        //         //   this.sort.active, this.sort.direction, this.paginator.pageIndex);
        //         return this.campsService!.getCamps();
        //       }),
        //       map(data => {
        //         // Flip flag to show that loading has finished.
        //         this.isLoadingResults = false;
        //         return data;
        //       }),
        //       catchError((error) => {
        //         this.isLoadingResults = false;
        //         console.log(error);
        //         return observableOf({});
        //       })
        //     ).subscribe(data => {  
        //       this.campsDataSource.data = JSON.parse(data); 
        //       this.dataSourceOriginal.data = JSON.parse(data);
        //       // this.dataSource.data = data; 
        //       console.log("received data");
        //       console.log(this.campsDataSource.data);
        //     });
    };
    CampsComponent.prototype.campClicked = function (camp) {
        console.log(camp);
        this.router.navigateByUrl('/camps/' + camp.camp_id + '/patients');
    };
    CampsComponent.prototype.getCampObject = function (camp) {
        console.log('camp', camp);
        var newCamp = new newcamp_model_1.Camp(camp.camp_id, camp['Camp held on'], camp.location_id, camp.project_id, camp.mandal_code, camp.coordinatorId, camp.opthamologistId, camp.camp_place, camp.latitude, camp.longitude);
        newCamp.state_id = camp.state_code;
        newCamp.district_id = camp.district_code;
        return newCamp;
    };
    CampsComponent.prototype.addCamp = function () {
        var dialogRef = this.dialog.open(newcamp_component_1.NewcampComponent, {
            panelClass: 'contact-form-dialog',
            data: {
                action: 'new'
            }
        });
        dialogRef.afterClosed().subscribe(function (result) {
            console.log("Dialog result: " + result);
        });
    };
    CampsComponent.prototype.editCamp = function (camp) {
        var dialogRef = this.dialog.open(newcamp_component_1.NewcampComponent, {
            panelClass: 'contact-form-dialog',
            data: {
                action: 'edit',
                camp: this.getCampObject(camp)
            }
        });
        dialogRef.afterClosed().subscribe(function (result) {
            console.log("Dialog result: " + result);
        });
    };
    __decorate([
        core_1.ViewChild(material_1.MatPaginator)
    ], CampsComponent.prototype, "paginator");
    __decorate([
        core_1.ViewChild(material_1.MatSort)
    ], CampsComponent.prototype, "sort");
    CampsComponent = __decorate([
        core_1.Component({
            selector: 'app-camps',
            templateUrl: './camps.component.html',
            styleUrls: ['./camps.component.scss'],
            animations: [animations_1.fuseAnimations]
        })
    ], CampsComponent);
    return CampsComponent;
}());
exports.CampsComponent = CampsComponent;
