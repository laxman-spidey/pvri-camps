"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var core_1 = require('@angular/core');
var network_service_1 = require('app/network.service');
var mock_models_1 = require('app/mock.models');
var CampsService = (function () {
    function CampsService(networkService) {
        this.networkService = networkService;
        this.mockModels = new mock_models_1.MockModels();
    }
    CampsService.prototype.getCamps = function () {
        return this.networkService.get(network_service_1.API.GET_CAMPS);
        //  return observableOf(this.mockModels.camps);
    };
    CampsService.prototype.getPatients = function (campId) {
        return this.networkService.post(network_service_1.API.GET_PATIENTS, { cid: campId });
        // return  observableOf(this.mockModels.patients);
    };
    CampsService.prototype.scheduleACamp = function (camp) {
        camp.camp_id = undefined;
        camp.location_id = undefined;
        var postData = {
            camp: JSON.stringify(camp)
        };
        return this.networkService.post(network_service_1.API.SCHEDULE_A_CAMP, postData);
    };
    CampsService.prototype.updateCampSchedule = function (camp) {
        camp.location_id = undefined;
        var postData = {
            camp: JSON.stringify(camp)
        };
        return this.networkService.post(network_service_1.API.UPDATE_CAMP_SCHEDULE, postData);
    };
    CampsService.prototype.createAPatient = function (patient, picture) {
        patient.patient_id = undefined;
        var postData = {
            patient: JSON.stringify(patient),
            picture: picture
        };
        return this.networkService.post(network_service_1.API.CREATE_A_PATIENT, postData);
    };
    CampsService.prototype.updatePatient = function (patient) {
        var postData = {
            camp: JSON.stringify(patient)
        };
        return this.networkService.post(network_service_1.API.UPDATE_PATIENT, postData);
    };
    CampsService.prototype.getScreeningsOfPatient = function (patientId) {
        var postData = {
            pid: patientId
        };
        return this.networkService.post(network_service_1.API.GET_SCREENINGS_OF_PATIENT, postData);
    };
    CampsService.prototype.updatePatientProfilePic = function (picBase64) {
        var postData = {
            picture: picBase64
        };
        return this.networkService.post(network_service_1.API.GET_SCREENINGS_OF_PATIENT, postData);
    };
    CampsService = __decorate([
        core_1.Injectable()
    ], CampsService);
    return CampsService;
}());
exports.CampsService = CampsService;
