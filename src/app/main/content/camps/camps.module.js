"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var core_1 = require('@angular/core');
var common_1 = require('@angular/common');
var camps_component_1 = require('./camps.component');
var network_service_1 = require('app/network.service');
var router_1 = require('@angular/router');
var camps_service_1 = require('./camps.service');
var shared_module_1 = require('@fuse/shared.module');
var table_1 = require('@angular/cdk/table');
var widget_module_1 = require('@fuse/components/widget/widget.module');
var animations_1 = require('@angular/platform-browser/animations');
var newpatient_component_1 = require('./patients/newpatient/newpatient.component');
var core_2 = require('@agm/core');
var material_1 = require('@angular/material');
var newcamp_component_1 = require('./newcamp/newcamp.component');
var http_1 = require('@angular/http');
var patients_component_1 = require('./patients/patients.component');
var routes = [
    {
        path: 'camps',
        redirectTo: 'camps/list'
    },
    {
        path: 'camps/list',
        component: camps_component_1.CampsComponent
    },
    {
        path: 'camps/newcamp',
        component: newcamp_component_1.NewcampComponent
    },
    {
        path: 'camps/:campid/patients',
        component: patients_component_1.PatientsComponent
    }
];
var CampsModule = (function () {
    function CampsModule() {
    }
    CampsModule = __decorate([
        core_1.NgModule({
            imports: [
                animations_1.BrowserAnimationsModule,
                common_1.CommonModule,
                router_1.RouterModule.forChild(routes),
                shared_module_1.FuseSharedModule,
                table_1.CdkTableModule,
                widget_module_1.FuseWidgetModule,
                http_1.HttpModule,
                //mat modules
                material_1.MatPaginatorModule,
                material_1.MatTableModule,
                material_1.MatButtonModule,
                material_1.MatIconModule,
                material_1.MatInputModule,
                material_1.MatFormFieldModule,
                material_1.MatDialogModule,
                material_1.MatDatepickerModule,
                material_1.MatNativeDateModule,
                material_1.MatProgressBarModule,
                material_1.MatProgressSpinnerModule,
                material_1.MatRippleModule,
                material_1.MatSortModule,
                material_1.MatListModule,
                material_1.MatSelectModule,
                material_1.MatToolbarModule,
                material_1.MatSlideToggleModule,
                core_2.AgmCoreModule.forRoot({
                    apiKey: 'AIzaSyCnDIHnV2BMiSRAZ2l_pnWKsf0WH-eJ7CM',
                    libraries: ['geometry']
                }),
            ],
            entryComponents: [newcamp_component_1.NewcampComponent, newpatient_component_1.NewpatientComponent],
            providers: [network_service_1.NetworkService, camps_service_1.CampsService, core_2.GoogleMapsAPIWrapper],
            declarations: [camps_component_1.CampsComponent, newcamp_component_1.NewcampComponent, patients_component_1.PatientsComponent, newpatient_component_1.NewpatientComponent]
        })
    ], CampsModule);
    return CampsModule;
}());
exports.CampsModule = CampsModule;
