"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var core_1 = require('@angular/core');
var router_1 = require('@angular/router');
require('rxjs/add/operator/filter');
require('rxjs/add/operator/map');
var index_1 = require('@fuse/animations/index');
var FuseContentComponent = (function () {
    function FuseContentComponent(router, activatedRoute, fuseConfig) {
        var _this = this;
        this.router = router;
        this.activatedRoute = activatedRoute;
        this.fuseConfig = fuseConfig;
        this.routeAnimationUp = false;
        this.routeAnimationDown = false;
        this.routeAnimationRight = false;
        this.routeAnimationLeft = false;
        this.routeAnimationFade = false;
        this.router.events
            .filter(function (event) { return event instanceof router_1.NavigationEnd; })
            .map(function () { return _this.activatedRoute; })
            .subscribe(function (event) {
            switch (_this.fuseSettings.routerAnimation) {
                case 'fadeIn':
                    _this.routeAnimationFade = !_this.routeAnimationFade;
                    break;
                case 'slideUp':
                    _this.routeAnimationUp = !_this.routeAnimationUp;
                    break;
                case 'slideDown':
                    _this.routeAnimationDown = !_this.routeAnimationDown;
                    break;
                case 'slideRight':
                    _this.routeAnimationRight = !_this.routeAnimationRight;
                    break;
                case 'slideLeft':
                    _this.routeAnimationLeft = !_this.routeAnimationLeft;
                    break;
            }
        });
        this.onConfigChanged =
            this.fuseConfig.onConfigChanged
                .subscribe(function (newSettings) {
                _this.fuseSettings = newSettings;
            });
    }
    FuseContentComponent.prototype.ngOnDestroy = function () {
        this.onConfigChanged.unsubscribe();
    };
    __decorate([
        core_1.HostBinding('@routerTransitionUp')
    ], FuseContentComponent.prototype, "routeAnimationUp");
    __decorate([
        core_1.HostBinding('@routerTransitionDown')
    ], FuseContentComponent.prototype, "routeAnimationDown");
    __decorate([
        core_1.HostBinding('@routerTransitionRight')
    ], FuseContentComponent.prototype, "routeAnimationRight");
    __decorate([
        core_1.HostBinding('@routerTransitionLeft')
    ], FuseContentComponent.prototype, "routeAnimationLeft");
    __decorate([
        core_1.HostBinding('@routerTransitionFade')
    ], FuseContentComponent.prototype, "routeAnimationFade");
    FuseContentComponent = __decorate([
        core_1.Component({
            selector: 'fuse-content',
            templateUrl: './content.component.html',
            styleUrls: ['./content.component.scss'],
            animations: index_1.fuseAnimations
        })
    ], FuseContentComponent);
    return FuseContentComponent;
}());
exports.FuseContentComponent = FuseContentComponent;
