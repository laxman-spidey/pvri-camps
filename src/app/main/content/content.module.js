"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var core_1 = require('@angular/core');
var router_1 = require('@angular/router');
var shared_module_1 = require('@fuse/shared.module');
var content_component_1 = require('app/main/content/content.component');
var dashboard_module_1 = require('app/main/content/dashboard/dashboard.module');
var camps_module_1 = require('app/main/content/camps/camps.module');
var FuseContentModule = (function () {
    function FuseContentModule() {
    }
    FuseContentModule = __decorate([
        core_1.NgModule({
            declarations: [
                content_component_1.FuseContentComponent
            ],
            imports: [
                router_1.RouterModule,
                shared_module_1.FuseSharedModule,
                dashboard_module_1.DashboardModule,
                camps_module_1.CampsModule,
            ],
            exports: [
                content_component_1.FuseContentComponent
            ]
        })
    ], FuseContentModule);
    return FuseContentModule;
}());
exports.FuseContentModule = FuseContentModule;
