import { Injectable } from '@angular/core';
import {Observable} from 'rxjs/Observable';
import {of} from 'rxjs/observable/of';
import {MockModels} from 'app/mock.models';
import { NetworkService, API} from 'app/network.service';

@Injectable()
export class DashboardService {

    private mockModels: MockModels;
    
    constructor(private networkService: NetworkService) { 
        this.mockModels = new MockModels();
    }
  
    getCampsMonthwise(): Observable<any>{
        // return this.networkService.sendRequest(API.GET_CAMPS_MONTHWISE);
        return of(this.mockModels.campsMonthwise);
    }
    getCampsYearwise(): Observable<any>{
        return this.networkService.get(API.GET_CAMPS_YEARWISE);
        // return of(this.mockModels.campsMonthwise);
    }
    getCampsYearwiseModel(): any {
        return JSON.parse(JSON.stringify(this.mockModels.campsMonthwise));
    }
    
    getCampsConducted(): Observable<any> {
        return this.networkService.get(API.GET_CAMPS_DASHBOARD);
        // return of(this.mockModels.campsConductedChart);
    }
    
    getCampsConductedPastWeek(): Observable<any> {
        return this.networkService.get(API.GET_CAMPS_PAST_WEEK);
        // return of(this.mockModels.campsConductedChart);
    }
    
    getCampsConductedModel(): Observable<any> {
        return JSON.parse(JSON.stringify(this.mockModels.campsConductedChart));
    }
    
    getScreeningsConducted(): Observable<any> {
        return this.networkService.get(API.GET_SCREENINGS_DASHBOARD);
    }
    getScreeningsConductedPastweek(): Observable<any> {
        return this.networkService.get(API.GET_SCREENINGS_PAST_WEEK);
    }
    getScreeningsConductedModel(): Observable<any> {
        return JSON.parse(JSON.stringify(this.mockModels.screeningsConductedChart));
    }
    getSurgeriesConducted(): Observable<any> {
        return this.networkService.get(API.GET_SURGERIES_DASHBOARD);
    }
    getSurgeriesConductedPastweek(): Observable<any> {
        return this.networkService.get(API.GET_SURGERIES_PAST_WEEK);
    }
    getSurgeriesConductedModel(): Observable<any> {
        return JSON.parse(JSON.stringify(this.mockModels.surgeriesConductedChart));
    }
    
    getVulnerables(days: number): Observable<any> {
        const postData = {
            'days': days
        };
        return this.networkService.post(API.GET_VULNERABLES, postData);
    }

    getVulnerablesModel(): Observable<any> {
        return JSON.parse(JSON.stringify(this.mockModels.vulnerables));
    }
    
    getHourlyScreeningsSurgeries(days: number): Observable<any> {
        const postData = {
            days: days
        };
        return this.networkService.post(API.GET_HOURLY_SCREENINGS_SURGERIES, postData);
    }
    getHourlyScreeningsSurgeriesModel(): Observable<any> {
    
        return JSON.parse(JSON.stringify(this.mockModels.HourlyScreeningsSurgeries));
    }
    
    getTopCoordinators(): Observable<any> {
        return this.networkService.get(API.GET_CAMP_COORDINATORS_PRODUCTIVITY);
    }
    getTopCoordinatorsModel(): Observable<any> {
        return JSON.parse(JSON.stringify(this.mockModels.topCoordinators));
    }
    
    getCampsConductedMap(): Observable<any> {
        return this.networkService.get(API.GET_CAMPS);
    }
    getCampsConductedMapModel(): Observable<any> {
        return JSON.parse(JSON.stringify(this.mockModels.campsConductedMap));
    }

    getDummyDevices(): Observable<any> {
        return of(this.mockModels.sales);
    }
    
    
    
}

