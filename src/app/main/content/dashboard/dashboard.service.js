"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var core_1 = require('@angular/core');
var of_1 = require('rxjs/observable/of');
var mock_models_1 = require('app/mock.models');
var network_service_1 = require('app/network.service');
var DashboardService = (function () {
    function DashboardService(networkService) {
        this.networkService = networkService;
        this.mockModels = new mock_models_1.MockModels();
    }
    DashboardService.prototype.getCampsMonthwise = function () {
        // return this.networkService.sendRequest(API.GET_CAMPS_MONTHWISE);
        return of_1.of(this.mockModels.campsMonthwise);
    };
    DashboardService.prototype.getCampsYearwise = function () {
        return this.networkService.get(network_service_1.API.GET_CAMPS_YEARWISE);
        // return of(this.mockModels.campsMonthwise);
    };
    DashboardService.prototype.getCampsYearwiseModel = function () {
        return JSON.parse(JSON.stringify(this.mockModels.campsMonthwise));
    };
    DashboardService.prototype.getCampsConducted = function () {
        return this.networkService.get(network_service_1.API.GET_CAMPS_DASHBOARD);
        // return of(this.mockModels.campsConductedChart);
    };
    DashboardService.prototype.getCampsConductedPastWeek = function () {
        return this.networkService.get(network_service_1.API.GET_CAMPS_PAST_WEEK);
        // return of(this.mockModels.campsConductedChart);
    };
    DashboardService.prototype.getCampsConductedModel = function () {
        return JSON.parse(JSON.stringify(this.mockModels.campsConductedChart));
    };
    DashboardService.prototype.getScreeningsConducted = function () {
        return this.networkService.get(network_service_1.API.GET_SCREENINGS_DASHBOARD);
    };
    DashboardService.prototype.getScreeningsConductedPastweek = function () {
        return this.networkService.get(network_service_1.API.GET_SCREENINGS_PAST_WEEK);
    };
    DashboardService.prototype.getScreeningsConductedModel = function () {
        return JSON.parse(JSON.stringify(this.mockModels.screeningsConductedChart));
    };
    DashboardService.prototype.getSurgeriesConducted = function () {
        return this.networkService.get(network_service_1.API.GET_SURGERIES_DASHBOARD);
    };
    DashboardService.prototype.getSurgeriesConductedPastweek = function () {
        return this.networkService.get(network_service_1.API.GET_SURGERIES_PAST_WEEK);
    };
    DashboardService.prototype.getSurgeriesConductedModel = function () {
        return JSON.parse(JSON.stringify(this.mockModels.surgeriesConductedChart));
    };
    DashboardService.prototype.getVulnerables = function (days) {
        var postData = {
            'days': days
        };
        return this.networkService.post(network_service_1.API.GET_VULNERABLES, postData);
    };
    DashboardService.prototype.getVulnerablesModel = function () {
        return JSON.parse(JSON.stringify(this.mockModels.vulnerables));
    };
    DashboardService.prototype.getPatientVisits = function () {
        return of_1.of(this.mockModels.patientVisits);
    };
    DashboardService.prototype.getHourlyScreeningsSurgeries = function (days) {
        var postData = {
            days: days
        };
        return this.networkService.post(network_service_1.API.GET_HOURLY_SCREENINGS_SURGERIES, postData);
    };
    DashboardService.prototype.getHourlyScreeningsSurgeriesModel = function () {
        return JSON.parse(JSON.stringify(this.mockModels.HourlyScreeningsSurgeries));
    };
    DashboardService.prototype.getTopCoordinators = function () {
        return this.networkService.get(network_service_1.API.GET_CAMP_COORDINATORS_PRODUCTIVITY);
    };
    DashboardService.prototype.getTopCoordinatorsModel = function () {
        return JSON.parse(JSON.stringify(this.mockModels.topCoordinators));
    };
    DashboardService.prototype.geCampsConductedMap = function () {
        return of_1.of(this.mockModels.campsConductedMap);
    };
    DashboardService.prototype.getDummyDevices = function () {
        return of_1.of(this.mockModels.sales);
    };
    DashboardService = __decorate([
        core_1.Injectable()
    ], DashboardService);
    return DashboardService;
}());
exports.DashboardService = DashboardService;
