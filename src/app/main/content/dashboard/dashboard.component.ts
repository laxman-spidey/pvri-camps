import { Component, ViewEncapsulation, OnInit } from '@angular/core';

import { DashboardService } from './dashboard.service';
import { fuseAnimations } from '@fuse/animations';
import { Router, ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/observable/forkJoin';

import {} from '@types/googlemaps';

@Component({
  // tslint:disable-next-line:component-selector
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss'],
  encapsulation: ViewEncapsulation.None,
  animations   : fuseAnimations
})
export class DashboardComponent implements OnInit {
    
    map: google.maps.Map;
    widgets: any;
    widget1SelectedYear = '2017';
    widget5SelectedDay = 'today';   
    
    campsYearwiseChart: any;
    campsYearwiseChartSelectedYear = '2017';
    campsYearwiseChartData: any;
    years: string[] = [];
    campsConductedChart: any;
    screeningsConductedChart: any;
    surgeriesConductedChart: any;
    vulnerables: any;
    hourlyScreeningsSurgeries: any;
    topCoordinators: any;
    campsConductedMap: any;
    dummyDevices: any;
  
    ngOnInit() {}
    debug(year: string) {
        console.log(year);
        console.log(this.campsYearwiseChartSelectedYear);
        this.campsYearwiseChart.activeData = this.campsYearwiseChart.datasets[year];
    }
    constructor(
        private dashboardService: DashboardService,
        private router: Router
    )
        {// Get the widgets from the service
        this.campsYearwiseChart = this.dashboardService.getCampsYearwiseModel();
        this.campsYearwiseChart.activeData = [{
                        label: 'camps',
                        data: [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
                        fill: 'start'
                    }];
        // this.campsYearwiseChartData = this.widgets.datasets["2017"];
        console.log(this.campsYearwiseChart);
        this.years = Object.keys(this.campsYearwiseChart.datasets);
        console.log(this.campsYearwiseChartData);
        this.campsYearwiseChart.isLoadingResults = true;
        this.dashboardService.getCampsYearwise().subscribe(response => {
            const data = response.data;
            const chartModel = this.dashboardService.getCampsYearwiseModel();
            chartModel.datasets = {};
            console.log('getCampsYearwise:', chartModel);     
            console.log('original:', this.dashboardService.getCampsYearwiseModel());

            // tslint:disable-next-line:forin
            for (const key in data) {
                if (chartModel.datasets[data[key].year] === undefined)
                {
                    chartModel.datasets['' + data[key].year] = [{
                        label: 'camps',
                        data: [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
                        fill: 'start'
                    }];
                }
                chartModel.datasets[data[key].year][0].data[chartModel.labels.indexOf(data[key ].month)] = data[key].camps;
                
            }
            this.years = Object.keys(chartModel.datasets);
            this.campsYearwiseChart.datasets = chartModel.datasets;
            this.campsYearwiseChart.isLoadingResults = false;
            this.campsYearwiseChart.activeData = this.campsYearwiseChart.datasets[this.years[0]];
            this.campsYearwiseChartSelectedYear = this.years[0];
        });


        
        this.campsConductedChart = this.dashboardService.getCampsConductedModel();
        this.dashboardService.getCampsConducted().subscribe(response => {
            this.campsConductedChart.camps.value = response.data.camps;
            this.campsConductedChart.camps.ofTarget = ((response.data.campsPastWeek - response.data.target) / response.data.target) * 100 ;
        });
        this.dashboardService.getCampsConductedPastWeek().subscribe(response => {
            const camps = [];
            const labels = [];
            // tslint:disable-next-line:forin
            for (const index in response.data) {
                camps[index] = parseInt(response.data[index].camps, 10);
                labels[index] = response.data[index].DayOfWeek;
            }
            this.campsConductedChart.datasets[0].data = camps;
            this.campsConductedChart.labels = labels;

            console.log('camps past week: ', this.campsConductedChart);
        });
        
        
        this.screeningsConductedChart = this.dashboardService.getScreeningsConductedModel(); 
        console.log('getScreeningsConducted : ', this.screeningsConductedChart);       
        this.dashboardService.getScreeningsConducted().subscribe(response => {            
            this.screeningsConductedChart.screenings.value = '' + response.data.screenings;
            this.screeningsConductedChart.screenings.ofTarget = ((response.data.screeningsPastWeek - response.data.target) / response.data.target) * 100 ;
            console.log('getScreeningsConducted: ', response);
        });
        this.dashboardService.getScreeningsConductedPastweek().subscribe(response => {            
            const Screenings = [];
            const labels = [];
            // tslint:disable-next-line:forin
            for (const index in response.data) {
                Screenings[index] = parseInt(response.data[index].Screenings, 10);
                labels[index] = response.data[index].DayOfWeek;
            }
            this.screeningsConductedChart.datasets[0].data = Screenings;
            this.screeningsConductedChart.labels = labels;
        });

        this.surgeriesConductedChart = this.dashboardService.getSurgeriesConductedModel();
        this.dashboardService.getSurgeriesConducted().subscribe(response => {
            console.log('surgeriesConductedChart', response);
            this.surgeriesConductedChart.surgeries.value = response.data.surgeries;
            this.surgeriesConductedChart.surgeries.ofTarget = ((response.data.surgeriesPastWeek - response.data.target) / response.data.target) * 100 ;
        });
        this.dashboardService.getSurgeriesConductedPastweek().subscribe(response => {            
            const Surgeries = [];
            const labels = [];
            // tslint:disable-next-line:forin
            for (const index in response.data) {
                Surgeries[index] = parseInt(response.data[index].Surgeries, 10);
                labels[index] = response.data[index].DayOfWeek;
            }
            this.surgeriesConductedChart.datasets[0].data = Surgeries;
            this.surgeriesConductedChart.labels = labels;
        });

        this.vulnerables = this.dashboardService.getVulnerablesModel();
        this.getVulnerables(this.vulnerables.days);
        
        this.hourlyScreeningsSurgeries = this.dashboardService.getHourlyScreeningsSurgeriesModel();
        Observable.forkJoin(
            this.dashboardService.getHourlyScreeningsSurgeries(1),
            this.dashboardService.getHourlyScreeningsSurgeries(0),
        ).subscribe(results => {
            
            const extractChartData = (result) => {
                const data = result.data;
                let screenings = [];
                let surgeries = [];
                let hours = [];
                // tslint:disable-next-line:forin
                for (const key in data) {
                    screenings.push(data[key].screenings);
                    surgeries.push(data[key].surgeries);
                    hours.push((data[key].hourOfDay <= 12) ? data[key].hourOfDay + ' AM' : (data[key].hourOfDay - 12) + ' PM' );
                }  
                if (data.length <= 0 )
                {
                    screenings = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0];
                    surgeries = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0];
                    hours = ['8 AM', '9 AM', '10 AM', '11 AM', '12 PM', '1 PM', '2 PM', '3 PM', '4 PM', '5 PM', '6 PM'];    
                }
                return {
                    hours: hours,
                    screenings: screenings,
                    surgeries: surgeries
                };
            };
            
            const yesterdayChart = extractChartData(results[0]);
            this.hourlyScreeningsSurgeries.labelsYesterday = yesterdayChart.hours;
            this.hourlyScreeningsSurgeries.datasets['yesterday'][0].data = yesterdayChart.screenings;
            this.hourlyScreeningsSurgeries.datasets['yesterday'][1].data = yesterdayChart.surgeries;

            const todayChart = extractChartData(results[1]);
            this.hourlyScreeningsSurgeries.labelsToday = todayChart.hours;
            this.hourlyScreeningsSurgeries.datasets['today'][0].data = todayChart.screenings;
            this.hourlyScreeningsSurgeries.datasets['today'][1].data = todayChart.surgeries;

            this.hourlyScreeningsSurgeries.activeDataset = this.hourlyScreeningsSurgeries.datasets['today'];
            this.hourlyScreeningsSurgeries.labels = this.hourlyScreeningsSurgeries.labelsToday;
            console.log('final hourly data: ', this.hourlyScreeningsSurgeries);    
            
        });
        
        // {employee_name: "Eswariah", employee_id: "124", screenings: "3667", surgeries: "24"}
        this.topCoordinators = this.dashboardService.getTopCoordinatorsModel();
        this.dashboardService.getTopCoordinators().subscribe(response => {
            const chartData = response.data;
            this.topCoordinators.rows = chartData;
            console.log(chartData);
        });
        
        this.campsConductedMap = this.dashboardService.getCampsConductedMapModel();
        this.dashboardService.getCampsConductedMap().subscribe(response => {
            const camps = response.data;
            const markers = [];
            for (const camp of camps) {
                const marker = {
                    lat: parseFloat(camp.latitude),
                    lng : parseFloat(camp.longitude),
                    label: camp.Camp
                };
                markers.push(marker);
            }
            this.campsConductedMap.markers = markers;
            this.campsConductedMap.bounds = this.generateBounds(markers);
            console.log('get camps map ', this.campsConductedMap);
        });
        
        this.dashboardService.getDummyDevices().subscribe(data => {
            this.dummyDevices = data; 
        });
        
        
        // Register the custom chart.js plugin
        this.registerCustomChartJSPlugin();
    }

    getVulnerables(days: number) {
        this.vulnerables.days = days;
        this.dashboardService.getVulnerables(this.vulnerables.days).subscribe(response => {
            console.log('getvulnerable: ', response);
            const data = response.data[0];
            const vulnerables = [];
            const men = {
                name  : 'Men',
                value : data.men,
                change: data.men_growth
            };
            vulnerables.push(men);
            const women = {
                name  : 'Women',
                value : data.women,
                change: data.women_growth
            };
            vulnerables.push(women);
            const children = {
                name  : 'Children',
                value : data.children,
                change: data.children_growth
            };
            vulnerables.push(children);
            const seniors = {
                name  : 'Seniors',
                value : data.seniors,
                change: data.seniors_growth
            };
            vulnerables.push(seniors);
            this.vulnerables.vulnerable = vulnerables;
            console.log('getVulnerables: ', this.vulnerables);
        });
    }
    updateVulnerables(value: any) {
        console.log('updating: ' + value);
        this.getVulnerables(parseInt(value, 10));
    }
    /**
     * Register a custom plugin
     */
    registerCustomChartJSPlugin()
    {
        (<any>window).Chart.plugins.register({
            afterDatasetsDraw: function (chart, easing) {
                // Only activate the plugin if it's made available
                // in the options
                if (
                    !chart.options.plugins.xLabelsOnTop ||
                    (chart.options.plugins.xLabelsOnTop && chart.options.plugins.xLabelsOnTop.active === false)
                )
                {
                    return;
                }

                // To only draw at the end of animation, check for easing === 1
                const ctx = chart.ctx;

                chart.data.datasets.forEach(function (dataset, i) {
                    const meta = chart.getDatasetMeta(i);
                    if ( !meta.hidden )
                    {
                        meta.data.forEach(function (element, index) {

                            // Draw the text in black, with the specified font
                            ctx.fillStyle = 'rgba(255, 255, 255, 0.7)';
                            const fontSize = 13;
                            const fontStyle = 'normal';
                            const fontFamily = 'Roboto, Helvetica Neue, Arial';
                            ctx.font = (<any>window).Chart.helpers.fontString(fontSize, fontStyle, fontFamily);

                            // Just naively convert to string for now
                            const dataString = dataset.data[index].toString() + 'k';

                            // Make sure alignment settings are correct
                            ctx.textAlign = 'center';
                            ctx.textBaseline = 'middle';
                            const padding = 15;
                            const startY = 24;
                            const position = element.tooltipPosition();
                            ctx.fillText(dataString, position.x, startY);

                            ctx.save();

                            ctx.beginPath();
                            ctx.setLineDash([5, 3]);
                            ctx.moveTo(position.x, startY + padding);
                            ctx.lineTo(position.x, position.y - padding);
                            ctx.strokeStyle = 'rgba(255,255,255,0.12)';
                            ctx.stroke();

                            ctx.restore();
                        });
                    }
                });
            }
        });
    }
    
    onMapReady(map) {
        this.map = map;
        const latlngbounds = new google.maps.LatLngBounds();
        
    }

    goToCamps() {
        this.router.navigateByUrl('/camps/list');    
    }


    generateBounds(markers): any {
        if (markers && markers.length > 0) {
            const bounds = new google.maps.LatLngBounds();

            markers.forEach((marker: any) => {
                bounds.extend(new google.maps.LatLng(marker.lat, marker.lng));
            });

            // check if there is only one marker
            if (bounds.getNorthEast().equals(bounds.getSouthWest())) {
                const extendPoint = new google.maps.LatLng(bounds.getNorthEast().lat() + 0.01, bounds.getNorthEast().lng() + 0.01);
                bounds.extend(extendPoint);
            }
            console.log('bounds', bounds);
            return bounds;
        }
        // return empty object when no bundle selected
        return {};
    }

}


