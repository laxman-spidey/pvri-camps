"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var core_1 = require('@angular/core');
var common_1 = require('@angular/common');
var dashboard_component_1 = require('./dashboard.component');
var router_1 = require('@angular/router');
var dashboard_service_1 = require('./dashboard.service');
var material_1 = require('@angular/material');
var core_2 = require('@agm/core');
var ng2_charts_1 = require('ng2-charts');
var ngx_charts_1 = require('@swimlane/ngx-charts');
var shared_module_1 = require('@fuse/shared.module');
var widget_module_1 = require('@fuse/components/widget/widget.module');
var routes = [
    {
        path: 'dashboard',
        component: dashboard_component_1.DashboardComponent
    }
];
var DashboardModule = (function () {
    function DashboardModule() {
    }
    DashboardModule = __decorate([
        core_1.NgModule({
            imports: [
                common_1.CommonModule,
                router_1.RouterModule.forChild(routes),
                material_1.MatFormFieldModule,
                material_1.MatIconModule,
                material_1.MatMenuModule,
                material_1.MatSelectModule,
                material_1.MatTabsModule,
                material_1.MatProgressBarModule,
                material_1.MatProgressSpinnerModule,
                core_2.AgmCoreModule.forRoot({
                    apiKey: 'AIzaSyD81ecsCj4yYpcXSLFcYU97PvRsE_X8Bx8'
                }),
                ng2_charts_1.ChartsModule,
                ngx_charts_1.NgxChartsModule,
                shared_module_1.FuseSharedModule,
                widget_module_1.FuseWidgetModule
            ],
            declarations: [dashboard_component_1.DashboardComponent],
            providers: [
                dashboard_service_1.DashboardService
            ]
        })
    ], DashboardModule);
    return DashboardModule;
}());
exports.DashboardModule = DashboardModule;
