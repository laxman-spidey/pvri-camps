"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var core_1 = require('@angular/core');
var animations_1 = require('@fuse/animations');
var Observable_1 = require('rxjs/Observable');
require('rxjs/add/observable/forkJoin');
var DashboardComponent = (function () {
    function DashboardComponent(dashboardService, router) {
        var _this = this;
        this.dashboardService = dashboardService;
        this.router = router;
        this.widget1SelectedYear = '2017';
        this.widget5SelectedDay = 'today';
        this.campsYearwiseChartSelectedYear = '2017';
        this.years = [];
        this.campsYearwiseChart = this.dashboardService.getCampsYearwiseModel();
        this.campsYearwiseChart.activeData = [{
                label: 'camps',
                data: [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
                fill: 'start'
            }];
        // this.campsYearwiseChartData = this.widgets.datasets["2017"];
        console.log(this.campsYearwiseChart);
        this.years = Object.keys(this.campsYearwiseChart.datasets);
        console.log(this.campsYearwiseChartData);
        this.campsYearwiseChart.isLoadingResults = true;
        this.dashboardService.getCampsYearwise().subscribe(function (response) {
            var data = response.data;
            var chartModel = _this.dashboardService.getCampsYearwiseModel();
            chartModel.datasets = {};
            console.log('getCampsYearwise:', chartModel);
            console.log('original:', _this.dashboardService.getCampsYearwiseModel());
            // tslint:disable-next-line:forin
            for (var key in data) {
                if (chartModel.datasets[data[key].year] === undefined) {
                    chartModel.datasets['' + data[key].year] = [{
                            label: 'camps',
                            data: [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
                            fill: 'start'
                        }];
                }
                chartModel.datasets[data[key].year][0].data[chartModel.labels.indexOf(data[key].month)] = data[key].camps;
            }
            _this.years = Object.keys(chartModel.datasets);
            _this.campsYearwiseChart.datasets = chartModel.datasets;
            _this.campsYearwiseChart.isLoadingResults = false;
            _this.campsYearwiseChart.activeData = _this.campsYearwiseChart.datasets[_this.years[0]];
            _this.campsYearwiseChartSelectedYear = _this.years[0];
        });
        this.campsConductedChart = this.dashboardService.getCampsConductedModel();
        this.dashboardService.getCampsConducted().subscribe(function (response) {
            _this.campsConductedChart.camps.value = response.data.camps;
            _this.campsConductedChart.camps.ofTarget = ((response.data.campsPastWeek - response.data.target) / response.data.target) * 100;
        });
        this.dashboardService.getCampsConductedPastWeek().subscribe(function (response) {
            var camps = [];
            var labels = [];
            // tslint:disable-next-line:forin
            for (var index in response.data) {
                camps[index] = parseInt(response.data[index].camps, 10);
                labels[index] = response.data[index].DayOfWeek;
            }
            _this.campsConductedChart.datasets[0].data = camps;
            _this.campsConductedChart.labels = labels;
            console.log('camps past week: ', _this.campsConductedChart);
        });
        this.screeningsConductedChart = this.dashboardService.getScreeningsConductedModel();
        console.log('getScreeningsConducted : ', this.screeningsConductedChart);
        this.dashboardService.getScreeningsConducted().subscribe(function (response) {
            _this.screeningsConductedChart.screenings.value = '' + response.data.screenings;
            _this.screeningsConductedChart.screenings.ofTarget = ((response.data.screeningsPastWeek - response.data.target) / response.data.target) * 100;
            console.log('getScreeningsConducted: ', response);
        });
        this.dashboardService.getScreeningsConductedPastweek().subscribe(function (response) {
            var Screenings = [];
            var labels = [];
            // tslint:disable-next-line:forin
            for (var index in response.data) {
                Screenings[index] = parseInt(response.data[index].Screenings, 10);
                labels[index] = response.data[index].DayOfWeek;
            }
            _this.screeningsConductedChart.datasets[0].data = Screenings;
            _this.screeningsConductedChart.labels = labels;
        });
        this.surgeriesConductedChart = this.dashboardService.getSurgeriesConductedModel();
        this.dashboardService.getSurgeriesConducted().subscribe(function (response) {
            console.log('surgeriesConductedChart', response);
            _this.surgeriesConductedChart.surgeries.value = response.data.surgeries;
            _this.surgeriesConductedChart.surgeries.ofTarget = ((response.data.surgeriesPastWeek - response.data.target) / response.data.target) * 100;
        });
        this.dashboardService.getSurgeriesConductedPastweek().subscribe(function (response) {
            var Surgeries = [];
            var labels = [];
            // tslint:disable-next-line:forin
            for (var index in response.data) {
                Surgeries[index] = parseInt(response.data[index].Surgeries, 10);
                labels[index] = response.data[index].DayOfWeek;
            }
            _this.surgeriesConductedChart.datasets[0].data = Surgeries;
            _this.surgeriesConductedChart.labels = labels;
        });
        this.vulnerables = this.dashboardService.getVulnerablesModel();
        this.getVulnerables(this.vulnerables.days);
        this.hourlyScreeningsSurgeries = this.dashboardService.getHourlyScreeningsSurgeriesModel();
        Observable_1.Observable.forkJoin(this.dashboardService.getHourlyScreeningsSurgeries(1), this.dashboardService.getHourlyScreeningsSurgeries(0)).subscribe(function (results) {
            _this.hourlyScreeningsSurgeries.datasets['yesterday'] = results[0].data;
            _this.hourlyScreeningsSurgeries.datasets['today'] = results[1].data;
            console.log(_this.hourlyScreeningsSurgeries);
        });
        //{employee_name: "Eswariah", employee_id: "124", screenings: "3667", surgeries: "24"}
        this.topCoordinators = this.dashboardService.getTopCoordinatorsModel();
        this.dashboardService.getTopCoordinators().subscribe(function (response) {
            var chartData = response.data;
            _this.topCoordinators.rows = chartData;
            console.log(chartData);
        });
        this.dashboardService.geCampsConductedMap().subscribe(function (data) {
            _this.campsConductedMap = data;
            console.log(_this.campsConductedMap);
        });
        this.dashboardService.getDummyDevices().subscribe(function (data) {
            _this.dummyDevices = data;
            console.log(_this.dummyDevices);
        });
        // Register the custom chart.js plugin
        this.registerCustomChartJSPlugin();
    }
    DashboardComponent.prototype.ngOnInit = function () { };
    DashboardComponent.prototype.debug = function (year) {
        console.log(year);
        console.log(this.campsYearwiseChartSelectedYear);
        this.campsYearwiseChart.activeData = this.campsYearwiseChart.datasets[year];
    };
    DashboardComponent.prototype.getVulnerables = function (days) {
        var _this = this;
        this.vulnerables.days = days;
        this.dashboardService.getVulnerables(this.vulnerables.days).subscribe(function (response) {
            console.log('getvulnerable: ', response);
            var data = response.data[0];
            var vulnerables = [];
            var men = {
                name: 'Men',
                value: data.men,
                change: data.men_growth
            };
            vulnerables.push(men);
            var women = {
                name: 'Women',
                value: data.women,
                change: data.women_growth
            };
            vulnerables.push(women);
            var children = {
                name: 'Children',
                value: data.children,
                change: data.children_growth
            };
            vulnerables.push(children);
            var seniors = {
                name: 'Seniors',
                value: data.seniors,
                change: data.seniors_growth
            };
            vulnerables.push(seniors);
            _this.vulnerables.vulnerable = vulnerables;
            console.log('getVulnerables: ', _this.vulnerables);
        });
    };
    DashboardComponent.prototype.updateVulnerables = function (value) {
        console.log('updating: ' + value);
        this.getVulnerables(parseInt(value, 10));
    };
    /**
     * Register a custom plugin
     */
    DashboardComponent.prototype.registerCustomChartJSPlugin = function () {
        window.Chart.plugins.register({
            afterDatasetsDraw: function (chart, easing) {
                // Only activate the plugin if it's made available
                // in the options
                if (!chart.options.plugins.xLabelsOnTop ||
                    (chart.options.plugins.xLabelsOnTop && chart.options.plugins.xLabelsOnTop.active === false)) {
                    return;
                }
                // To only draw at the end of animation, check for easing === 1
                var ctx = chart.ctx;
                chart.data.datasets.forEach(function (dataset, i) {
                    var meta = chart.getDatasetMeta(i);
                    if (!meta.hidden) {
                        meta.data.forEach(function (element, index) {
                            // Draw the text in black, with the specified font
                            ctx.fillStyle = 'rgba(255, 255, 255, 0.7)';
                            var fontSize = 13;
                            var fontStyle = 'normal';
                            var fontFamily = 'Roboto, Helvetica Neue, Arial';
                            ctx.font = window.Chart.helpers.fontString(fontSize, fontStyle, fontFamily);
                            // Just naively convert to string for now
                            var dataString = dataset.data[index].toString() + 'k';
                            // Make sure alignment settings are correct
                            ctx.textAlign = 'center';
                            ctx.textBaseline = 'middle';
                            var padding = 15;
                            var startY = 24;
                            var position = element.tooltipPosition();
                            ctx.fillText(dataString, position.x, startY);
                            ctx.save();
                            ctx.beginPath();
                            ctx.setLineDash([5, 3]);
                            ctx.moveTo(position.x, startY + padding);
                            ctx.lineTo(position.x, position.y - padding);
                            ctx.strokeStyle = 'rgba(255,255,255,0.12)';
                            ctx.stroke();
                            ctx.restore();
                        });
                    }
                });
            }
        });
    };
    DashboardComponent.prototype.goToCamps = function () {
        this.router.navigateByUrl('/camps/list');
    };
    DashboardComponent = __decorate([
        core_1.Component({
            // tslint:disable-next-line:component-selector
            selector: 'app-dashboard',
            templateUrl: './dashboard.component.html',
            styleUrls: ['./dashboard.component.scss'],
            encapsulation: core_1.ViewEncapsulation.None,
            animations: animations_1.fuseAnimations
        })
    ], DashboardComponent);
    return DashboardComponent;
}());
exports.DashboardComponent = DashboardComponent;
