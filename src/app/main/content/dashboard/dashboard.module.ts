import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DashboardComponent } from './dashboard.component';
import { RouterModule, Routes } from '@angular/router';
import {DashboardService} from './dashboard.service';

import { 
  MatFormFieldModule, 
  MatIconModule, 
  MatMenuModule, 
  MatSelectModule, 
  MatTabsModule, 
  MatProgressBarModule,
  MatProgressSpinnerModule, 
  
} from '@angular/material';

import { AgmCoreModule } from '@agm/core';
import { ChartsModule } from 'ng2-charts';
import { NgxChartsModule } from '@swimlane/ngx-charts';

import { FuseSharedModule } from '@fuse/shared.module';
import { FuseWidgetModule } from '@fuse/components/widget/widget.module';


const routes: Routes = [
    {
        path     : 'dashboard',
        component: DashboardComponent,
    }
];


@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    MatFormFieldModule,
    MatIconModule,
    MatMenuModule,
    MatSelectModule,
    MatTabsModule,
    MatProgressBarModule,
    MatProgressSpinnerModule,

    AgmCoreModule.forRoot({
        apiKey: 'AIzaSyD81ecsCj4yYpcXSLFcYU97PvRsE_X8Bx8'
    }),
    ChartsModule,
    NgxChartsModule,

    FuseSharedModule,
    FuseWidgetModule
  ],
  declarations: [DashboardComponent],
  providers : [
    DashboardService  
  ]
  
})
export class DashboardModule { }
