import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { FuseSharedModule } from '@fuse/shared.module';

import { FuseContentComponent } from 'app/main/content/content.component';
import {DashboardModule} from 'app/main/content/dashboard/dashboard.module';
import {CampsModule} from 'app/main/content/camps/camps.module';

@NgModule({
    declarations: [
        FuseContentComponent
    ],
    imports     : [
        RouterModule,

        FuseSharedModule,
        DashboardModule,
        CampsModule,
    ],
    exports: [
        FuseContentComponent
    ]
})
export class FuseContentModule
{
}
