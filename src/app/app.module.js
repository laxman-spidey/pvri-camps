"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var core_1 = require('@angular/core');
var platform_browser_1 = require('@angular/platform-browser');
var http_1 = require('@angular/common/http');
var animations_1 = require('@angular/platform-browser/animations');
var router_1 = require('@angular/router');
var core_2 = require('@ngx-translate/core');
require('hammerjs');
var fuse_module_1 = require('@fuse/fuse.module');
var shared_module_1 = require('@fuse/shared.module');
var fuse_config_1 = require('./fuse-config');
var app_component_1 = require('./app.component');
var main_module_1 = require('./main/main.module');
var sample_module_1 = require('./main/content/sample/sample.module');
var login_module_1 = require('./login/login.module');
var auth_service_1 = require('./login/auth.service');
var network_service_1 = require('./network.service');
var appRoutes = [
    {
        path: '**',
        redirectTo: 'dashboard'
    },
    {
        path: 'auth/login',
        redirectTo: 'login'
    }
];
var AppModule = (function () {
    function AppModule() {
    }
    AppModule = __decorate([
        core_1.NgModule({
            declarations: [
                app_component_1.AppComponent
            ],
            imports: [
                platform_browser_1.BrowserModule,
                animations_1.BrowserAnimationsModule,
                http_1.HttpClientModule,
                router_1.RouterModule.forRoot(appRoutes),
                core_2.TranslateModule.forRoot(),
                // Fuse Main and Shared modules
                fuse_module_1.FuseModule.forRoot(fuse_config_1.fuseConfig),
                shared_module_1.FuseSharedModule,
                main_module_1.FuseMainModule,
                sample_module_1.FuseSampleModule,
                login_module_1.LoginModule,
            ],
            bootstrap: [
                app_component_1.AppComponent
            ],
            providers: [
                auth_service_1.AuthService,
                network_service_1.NetworkService
            ]
        })
    ], AppModule);
    return AppModule;
}());
exports.AppModule = AppModule;
