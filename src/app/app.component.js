"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var core_1 = require('@angular/core');
var en_1 = require('./navigation/i18n/en');
var tr_1 = require('./navigation/i18n/tr');
var AppComponent = (function () {
    function AppComponent(translate, fuseNavigationService, fuseSplashScreen, fuseTranslationLoader, authService, router) {
        this.translate = translate;
        this.fuseNavigationService = fuseNavigationService;
        this.fuseSplashScreen = fuseSplashScreen;
        this.fuseTranslationLoader = fuseTranslationLoader;
        this.authService = authService;
        this.router = router;
        this.loggedIn = false;
        // Add languages
        this.translate.addLangs(['en', 'tr']);
        // Set the default language
        this.translate.setDefaultLang('en');
        // Set the navigation translations
        this.fuseTranslationLoader.loadTranslations(en_1.locale, tr_1.locale);
        // Use a language
        this.translate.use('en');
    }
    AppComponent.prototype.ngOnInit = function () {
        //authorize first time the app is opened.
        // this.authorize(this.authService.getLoggedInStatus());
        var _this = this;
        this.authService.loginUpdate.subscribe(function (data) {
            _this.loggedIn = data;
            if (!_this.loggedIn) {
                _this.router.navigateByUrl('/auth/login');
            }
        });
    };
    AppComponent.prototype.authorize = function (data) {
        this.loggedIn = data;
        if (!this.loggedIn) {
            this.router.navigateByUrl('/auth/login');
        }
    };
    AppComponent = __decorate([
        core_1.Component({
            selector: 'fuse-root',
            templateUrl: './app.component.html',
            styleUrls: ['./app.component.scss']
        })
    ], AppComponent);
    return AppComponent;
}());
exports.AppComponent = AppComponent;
