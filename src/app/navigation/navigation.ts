export const navigation = [
    {
        'id'      : 'applications',
        'title'   : 'Applications',
        'translate': 'NAV.APPLICATIONS',
        'type'    : 'group',
        'children': [
            {
                'id'   : 'dashboard',
                'title': 'Dashboard',
                'type' : 'item',
                'icon' : 'dashboard',
                'url'  : '/dashboard',
                'badge': {
                    'title': 0,
                    'translate': 'NAV.APP.BADGE',
                    'bg'   : '#F44336',
                    'fg'   : '#FFFFFF'
                }
            },
            {
                'id'   : 'camps',
                'title': 'Camps',
                'type' : 'collapse',
                'icon' : 'email',
                'children' : [
                    {
                        'id'   : 'List',
                        'title': 'List',
                        'type' : 'item',
                        'icon' : 'dashboard',
                        'url'  : '/camps/list',
                    },
                    {
                        'id'   : 'newcamp',
                        'title': 'Schedule A Camp',
                        'type' : 'item',
                        'icon' : 'dashboard',
                        'url'  : '/camps/newcamp',
                    },
                    {
                        'id'   : 'patients',
                        'title': 'Patient',
                        'type' : 'item',
                        'icon' : 'dashboard',
                        'url'  : '/camps/number/patients',
                    }
                ]
            }
        ]
    }
];
