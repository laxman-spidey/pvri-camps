export const locale = {
    lang: 'en',
    data: {
        'NAV': {
            'APPLICATIONS': 'Applications',
            'APP'  : {
                'TITLE': 'Dashboard',
                'BADGE': '25'
            }
        }
    }
};
