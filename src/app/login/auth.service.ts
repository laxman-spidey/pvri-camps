import { Injectable } from '@angular/core';
import {md5} from 'md5';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs/Observable';
import {of} from 'rxjs/observable/of';
import { environment } from '../../environments/environment';
import { Subject } from 'rxjs/Subject';
import {Response} from '../response.model';
// import {API} from '../network.service';



@Injectable()
export class AuthService {

    loggedIn: boolean;
    loginUpdate: Subject<boolean> = new Subject<boolean>();
    
    constructor(private http: HttpClient) { 
        if (this.getLoggedInStatus != null) {
            this.loggedIn = false;  
        }
        else {
            this.loggedIn = true;
        }
        
    }

    login(username: string, password: string): Observable<any> {
        // var hashPassword = this.md5(password);
        const postParams = {
            username: username,
            password: password
        };
        const url = environment.apiUrl + '/pvri/auth/token';
        const response = this.http.post(url, postParams).map((res: any) => {
            console.log(res);
            const data = res.data;
            if (res.response_code === 200) {
                this.loginSuccess(data);
                of(res);
            }
            this.loginSuccess(res.data);
            return res;
        });
        return response;
        // return of({});
    }
    
    
    
    public isLoggedIn(): Observable<boolean> {
        return of(this.getLoggedInStatus());
    }
    public getLoggedInStatus(): boolean {
        if (this.getToken() != null) {
            this.loggedIn = false;  
        }
        else {
            this.loggedIn = true;
        }
        return this.loggedIn;
    }
    
    changeLoginState(loginState: boolean) {
        this.loggedIn = loginState;
        this.loginUpdate.next(this.loggedIn);
    }
    loginSuccess(token: string) {
        console.log('isLoggedIn=true');
        window.sessionStorage.setItem('token', token);
        this.changeLoginState(true);
    }
    
    loggedOut() {
        console.log('isLoggedIn=false');
        window.sessionStorage.removeItem('token');
        this.changeLoginState(false);
    }
    
    getToken(): string {
        return window.sessionStorage.getItem('token');
    }
    
    logout() {
        this.loggedOut();
        const postParams = {
            session_id : this.getToken()
        };
        const url = environment.apiUrl + '/pvri/login/logout';
        const response = this.http.post(url, postParams).subscribe((res: any) => {
            console.log('loggedOut');
            return res;
        });
        return response;
    }
}
