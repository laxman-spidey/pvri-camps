"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var core_1 = require('@angular/core');
var forms_1 = require('@angular/forms');
var animations_1 = require('@fuse/animations');
var FuseLoginComponent = (function () {
    function FuseLoginComponent(fuseConfig, formBuilder, authService, router) {
        this.fuseConfig = fuseConfig;
        this.formBuilder = formBuilder;
        this.authService = authService;
        this.router = router;
        this.fuseConfig.setConfig({
            layout: {
                navigation: 'none',
                toolbar: 'none',
                footer: 'none'
            }
        });
        this.loginFormErrors = {
            email: {},
            password: {}
        };
    }
    FuseLoginComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.loginForm = this.formBuilder.group({
            email: ['', [forms_1.Validators.required, forms_1.Validators.email]],
            password: ['', forms_1.Validators.required]
        });
        this.loginForm.valueChanges.subscribe(function () {
            _this.onLoginFormValuesChanged();
        });
    };
    FuseLoginComponent.prototype.onLoginFormValuesChanged = function () {
        // tslint:disable-next-line:forin
        for (var field in this.loginFormErrors) {
            if (!this.loginFormErrors.hasOwnProperty(field)) {
                continue;
            }
            // Clear previous errors
            this.loginFormErrors[field] = {};
            // Get the control
            var control = this.loginForm.get(field);
            if (control && control.dirty && !control.valid) {
                console.log('error ' + control.errors);
                this.loginFormErrors[field] = control.errors;
            }
        }
    };
    FuseLoginComponent.prototype.onLoginClicked = function () {
        var _this = this;
        console.log('onLoginClicked');
        this.authService.login(this.email, this.password).subscribe(function (response) {
            console.log(response);
            if (response !== undefined && response !== null) {
                if (response.response_code === '200') {
                    var token = response.token;
                    _this.router.navigateByUrl('/dashboard');
                }
                else {
                    alert('invalid credentials');
                }
            }
        });
    };
    FuseLoginComponent = __decorate([
        core_1.Component({
            selector: 'fuse-login',
            templateUrl: './login.component.html',
            styleUrls: ['./login.component.scss'],
            animations: animations_1.fuseAnimations
        })
    ], FuseLoginComponent);
    return FuseLoginComponent;
}());
exports.FuseLoginComponent = FuseLoginComponent;
