"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var core_1 = require('@angular/core');
var of_1 = require('rxjs/observable/of');
var environment_1 = require('../../environments/environment');
var Subject_1 = require('rxjs/Subject');
// import {API} from '../network.service';
var AuthService = (function () {
    function AuthService(http) {
        this.http = http;
        this.loginUpdate = new Subject_1.Subject();
        if (this.getLoggedInStatus != null) {
            this.loggedIn = false;
        }
        else {
            this.loggedIn = true;
        }
    }
    AuthService.prototype.login = function (username, password) {
        var _this = this;
        // var hashPassword = this.md5(password);
        var postParams = {
            username: username,
            password: password
        };
        var url = environment_1.environment.apiUrl + '/pvri/auth/token';
        var response = this.http.post(url, postParams).map(function (res) {
            console.log(res);
            var data = res.data;
            if (res.response_code === 200) {
                _this.loginSuccess(data);
                of_1.of(res);
            }
            _this.loginSuccess(res.data);
            return res;
        });
        return response;
        // return of({});
    };
    AuthService.prototype.isLoggedIn = function () {
        return of_1.of(this.getLoggedInStatus());
    };
    AuthService.prototype.getLoggedInStatus = function () {
        if (this.getToken() != null) {
            this.loggedIn = false;
        }
        else {
            this.loggedIn = true;
        }
        return this.loggedIn;
    };
    AuthService.prototype.changeLoginState = function (loginState) {
        this.loggedIn = loginState;
        this.loginUpdate.next(this.loggedIn);
    };
    AuthService.prototype.loginSuccess = function (token) {
        console.log('isLoggedIn=true');
        window.sessionStorage.setItem('token', token);
        this.changeLoginState(true);
    };
    AuthService.prototype.loggedOut = function () {
        console.log('isLoggedIn=false');
        window.sessionStorage.removeItem('token');
        this.changeLoginState(false);
    };
    AuthService.prototype.getToken = function () {
        return window.sessionStorage.getItem('token');
    };
    AuthService.prototype.logout = function () {
        this.loggedOut();
        var postParams = {
            session_id: this.getToken()
        };
        var url = environment_1.environment.apiUrl + '/pvri/login/logout';
        var response = this.http.post(url, postParams).subscribe(function (res) {
            console.log('loggedOut');
            return res;
        });
        return response;
    };
    AuthService = __decorate([
        core_1.Injectable()
    ], AuthService);
    return AuthService;
}());
exports.AuthService = AuthService;
