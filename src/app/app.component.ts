import { Component, OnInit } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';

import { FuseSplashScreenService } from '@fuse/services/splash-screen.service';
import { FuseTranslationLoaderService } from '@fuse/services/translation-loader.service';
import { FuseNavigationService } from '@fuse/components/navigation/navigation.service';

import { locale as navigationEnglish } from './navigation/i18n/en';
import { locale as navigationTurkish } from './navigation/i18n/tr';
import { AuthService } from 'app/login/auth.service';
import { Router } from '@angular/router';

@Component({
    selector   : 'fuse-root',
    templateUrl: './app.component.html',
    styleUrls  : ['./app.component.scss']
})
export class AppComponent implements OnInit 
{
    private loggedIn : boolean = false;
    constructor(
        private translate: TranslateService,
        private fuseNavigationService: FuseNavigationService,
        private fuseSplashScreen: FuseSplashScreenService,
        private fuseTranslationLoader: FuseTranslationLoaderService,
        private authService : AuthService,
        private router : Router,
    )
    {
        // Add languages
        this.translate.addLangs(['en', 'tr']);

        // Set the default language
        this.translate.setDefaultLang('en');

        // Set the navigation translations
        this.fuseTranslationLoader.loadTranslations(navigationEnglish, navigationTurkish);

        // Use a language
        this.translate.use('en');
        
        
    }
    
    ngOnInit() {
        //authorize first time the app is opened.
        // this.authorize(this.authService.getLoggedInStatus());
        
        this.authService.loginUpdate.subscribe( data => {
            this.loggedIn = data;
            if(!this.loggedIn) {
                this.router.navigateByUrl('/auth/login');  
            }
        });  
    }
    
    authorize(data: boolean) {
        this.loggedIn = data;
        if(!this.loggedIn) {
            this.router.navigateByUrl('/auth/login');  
        }
    }
}
