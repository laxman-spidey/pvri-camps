import { Injectable } from '@angular/core';
import { environment } from '../environments/environment';
import {Response} from './response.model';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Observable} from 'rxjs/Observable';
import {of} from 'rxjs/observable/of';
import {AuthService} from 'app/login/auth.service';
import { Router } from '@angular/router';



@Injectable()
export class NetworkService {

    
  constructor(  private http: HttpClient,
                private authService: AuthService,
                private router: Router) { }

    getAPIUrl(api: string): string {
        return environment.apiUrl + api;
    }
    
    post(api: string, postParams: any = {}): Observable<any> {
        const token = this.authService.getToken();
        const httpOptions = {
         headers: new HttpHeaders({
            'Authorization': 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpZCI6IjkifQ.Zc0CPcFtXSCkgHo9mhg3ogCD_GxjI0hnutFWVv-5FMg'
          })
        };
        console.log('POST :' + this.getAPIUrl(api), httpOptions);
        console.log('POST Params: ', postParams);
        const response = this.http.post(this.getAPIUrl(api), postParams, httpOptions).map((res) => {
            // if(data.response == 401) {
            //     authService.logout();
            //     return {};
            // } 
            // else {
            //     return res;
            // }
            console.log('POST '+this.getAPIUrl(api), res);
            return res;
        });
        return response;
    }
    
    get(api: string): Observable<any> {
        const token = this.authService.getToken();
        const httpOptions = {
         headers: new HttpHeaders({
            'Authorization': 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpZCI6IjkifQ.Zc0CPcFtXSCkgHo9mhg3ogCD_GxjI0hnutFWVv-5FMg'
          })
        };
        console.log('GET :' + this.getAPIUrl(api), httpOptions);
        const response = this.http.get(this.getAPIUrl(api), httpOptions).map((res) => {
            console.log(res);
            // if(data.response == 401) {
            //     authService.logout();
            //     return {};
            // } 
            // else {
            //     return res;
            // }
            console.log('GET '+this.getAPIUrl(api), res);
            return res;
        });
        return response;
    }
}

export const API = {
    // AUTHORIZE : '/pvri/login/validate_credentials',
    AUTHORIZE : '/pvri/auth/token',
    GET_CAMPS_MONTHWISE : '/pvri/camps/getCampsMonthWise',
    
    GET_SCREENING_SURGERIES : '/pvri/patients/getScreeningsSurgeries',
    GET_EXAMINATIONS : '/pvri/patients/getExaminations',
    
    GET_GENDER_SCREENINGS : '/pvri/patients/genderWiseScreening',
    
    // dashboard APIs
    GET_CAMPS_YEARWISE : '/pvri/camps/getCampsYearWise',
    
    GET_CAMPS_DASHBOARD : '/pvri/camps/getCampsDashboard',
    GET_CAMPS_PAST_WEEK: '/pvri/camps/getCampsPastWeek',
    
    GET_SCREENINGS_DASHBOARD : '/pvri/camps/getScreeningsDashboard',
    GET_SCREENINGS_PAST_WEEK: '/pvri/patients/getScreeningsPastweek',
     
    GET_SURGERIES_DASHBOARD : '/pvri/camps/getSurgeriesDashboard',
    GET_SURGERIES_PAST_WEEK: '/pvri/patients/getSurgeriesPastweek', 
    GET_VULNERABLES : '/pvri/patients/getVulnerables',
    GET_CAMP_COORDINATORS_PRODUCTIVITY: '/pvri/hierarchy/getCampCoordinatorsProductivity',
    GET_HOURLY_SCREENINGS_SURGERIES: '/pvri/visits/hourlyScreeningsSurgeries',

    // Camp APIs
    GET_PATIENTS: '/pvri/patients/getPatients',
    GET_CAMPS: '/pvri/camps/getCamps',
    
    // Hierarchy APIs
    GET_STATES: '/pvri/hierarchy/getStates/',
    GET_DISTRICTS: '/pvri/hierarchy/getDistricts/',
    GET_MANDALS: '/pvri/hierarchy/getMandals/',
    GET_LOCATIONS: '/pvri/hierarchy/getLocations/',
    GET_PROJECTS: '/pvri/camps/getProjects/',
    GET_CAMP_COORDINATORS: '/pvri/camps/getCampCoordinators/',
    GET_CAMP_OPTIMETRICIANS: '/pvri/camps/getOptometricAssistants/',
    
    
    SCHEDULE_A_CAMP : '/pvri/camps/scheduleACamp',
    UPDATE_CAMP_SCHEDULE : '/pvri/camps/changeCampSchedule',
    CREATE_A_PATIENT: '/pvri/patients/newPatient',
    UPDATE_PATIENT: '/pvri/patients/updatePatient',
    GET_SCREENINGS_OF_PATIENT: '/pvri/patients/getScreeningsPerPatient ',
    UPDATE_PATIENT_PROFILE_PIC: '/pvri/patients/changeProfilePic',
    CREATE_A_SCREENING: '/pvri/patients/newscreening',
};
