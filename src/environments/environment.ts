// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `.angular-cli.json`.

export const environment = {
    production: false,
    hmr       : false,
    apiUrl : 'https://exp-node-mittu-spidey.c9users.io'
    // apiUrl : 'http://159.89.161.27'
    // apiUrl : "https://pvri-server-mittu-spidey.c9users.io/pvri-server-dev"
};
