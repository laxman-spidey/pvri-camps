"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var core_1 = require('@angular/core');
var FuseMatSidenavHelperDirective = (function () {
    function FuseMatSidenavHelperDirective(fuseMatSidenavService, fuseMatchMedia, observableMedia, matSidenav) {
        this.fuseMatSidenavService = fuseMatSidenavService;
        this.fuseMatchMedia = fuseMatchMedia;
        this.observableMedia = observableMedia;
        this.matSidenav = matSidenav;
        this.isLockedOpen = true;
    }
    FuseMatSidenavHelperDirective.prototype.ngOnInit = function () {
        var _this = this;
        this.fuseMatSidenavService.setSidenav(this.id, this.matSidenav);
        if (this.observableMedia.isActive(this.matIsLockedOpenBreakpoint)) {
            this.isLockedOpen = true;
            this.matSidenav.mode = 'side';
            this.matSidenav.toggle(true);
        }
        else {
            this.isLockedOpen = false;
            this.matSidenav.mode = 'over';
            this.matSidenav.toggle(false);
        }
        this.matchMediaSubscription = this.fuseMatchMedia.onMediaChange.subscribe(function () {
            if (_this.observableMedia.isActive(_this.matIsLockedOpenBreakpoint)) {
                _this.isLockedOpen = true;
                _this.matSidenav.mode = 'side';
                _this.matSidenav.toggle(true);
            }
            else {
                _this.isLockedOpen = false;
                _this.matSidenav.mode = 'over';
                _this.matSidenav.toggle(false);
            }
        });
    };
    FuseMatSidenavHelperDirective.prototype.ngOnDestroy = function () {
        this.matchMediaSubscription.unsubscribe();
    };
    __decorate([
        core_1.HostBinding('class.mat-is-locked-open')
    ], FuseMatSidenavHelperDirective.prototype, "isLockedOpen");
    __decorate([
        core_1.Input('fuseMatSidenavHelper')
    ], FuseMatSidenavHelperDirective.prototype, "id");
    __decorate([
        core_1.Input('mat-is-locked-open')
    ], FuseMatSidenavHelperDirective.prototype, "matIsLockedOpenBreakpoint");
    FuseMatSidenavHelperDirective = __decorate([
        core_1.Directive({
            selector: '[fuseMatSidenavHelper]'
        })
    ], FuseMatSidenavHelperDirective);
    return FuseMatSidenavHelperDirective;
}());
exports.FuseMatSidenavHelperDirective = FuseMatSidenavHelperDirective;
var FuseMatSidenavTogglerDirective = (function () {
    function FuseMatSidenavTogglerDirective(fuseMatSidenavService) {
        this.fuseMatSidenavService = fuseMatSidenavService;
    }
    FuseMatSidenavTogglerDirective.prototype.onClick = function () {
        this.fuseMatSidenavService.getSidenav(this.id).toggle();
    };
    __decorate([
        core_1.Input('fuseMatSidenavToggler')
    ], FuseMatSidenavTogglerDirective.prototype, "id");
    __decorate([
        core_1.HostListener('click')
    ], FuseMatSidenavTogglerDirective.prototype, "onClick");
    FuseMatSidenavTogglerDirective = __decorate([
        core_1.Directive({
            selector: '[fuseMatSidenavToggler]'
        })
    ], FuseMatSidenavTogglerDirective);
    return FuseMatSidenavTogglerDirective;
}());
exports.FuseMatSidenavTogglerDirective = FuseMatSidenavTogglerDirective;
