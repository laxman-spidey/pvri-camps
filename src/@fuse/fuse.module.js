"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
var core_1 = require('@angular/core');
var config_service_1 = require('@fuse/services/config.service');
var copier_service_1 = require('@fuse/services/copier.service');
var match_media_service_1 = require('@fuse/services/match-media.service');
var fuse_mat_sidenav_service_1 = require('@fuse/directives/fuse-mat-sidenav/fuse-mat-sidenav.service');
var navigation_service_1 = require('@fuse/components/navigation/navigation.service');
var sidebar_service_1 = require('@fuse/components/sidebar/sidebar.service');
var splash_screen_service_1 = require('@fuse/services/splash-screen.service');
var translation_loader_service_1 = require('@fuse/services/translation-loader.service');
var FuseModule = (function () {
    function FuseModule(parentModule) {
        if (parentModule) {
            throw new Error('FuseModule is already loaded. Import it in the AppModule only!');
        }
    }
    FuseModule.forRoot = function (config) {
        return {
            ngModule: FuseModule,
            providers: [
                {
                    provide: config_service_1.FUSE_CONFIG,
                    useValue: config
                }
            ]
        };
    };
    FuseModule = __decorate([
        core_1.NgModule({
            entryComponents: [],
            providers: [
                config_service_1.FuseConfigService,
                copier_service_1.FuseCopierService,
                match_media_service_1.FuseMatchMediaService,
                fuse_mat_sidenav_service_1.FuseMatSidenavHelperService,
                navigation_service_1.FuseNavigationService,
                sidebar_service_1.FuseSidebarService,
                splash_screen_service_1.FuseSplashScreenService,
                translation_loader_service_1.FuseTranslationLoaderService
            ]
        }),
        __param(0, core_1.Optional()),
        __param(0, core_1.SkipSelf())
    ], FuseModule);
    return FuseModule;
}());
exports.FuseModule = FuseModule;
