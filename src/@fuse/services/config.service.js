"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
var core_1 = require('@angular/core');
var router_1 = require('@angular/router');
var BehaviorSubject_1 = require('rxjs/BehaviorSubject');
// Define the default config
var DEFAULT_CONFIG = {
    layout: {
        navigation: 'left',
        navigationFolded: false,
        toolbar: 'below',
        footer: 'below',
        mode: 'fullwidth' // 'boxed', 'fullwidth'
    },
    colorClasses: {
        toolbar: 'mat-white-500-bg',
        navbar: 'mat-fuse-dark-700-bg',
        footer: 'mat-fuse-dark-900-bg'
    },
    customScrollbars: true,
    routerAnimation: 'fadeIn' // fadeIn, slideUp, slideDown, slideRight, slideLeft, none
};
// Create the injection token for the custom config
exports.FUSE_CONFIG = new core_1.InjectionToken('fuseCustomConfig');
var FuseConfigService = (function () {
    /**
     * Constructor
     *
     * @param router
     * @param platform
     * @param config
     */
    function FuseConfigService(router, platform, config) {
        var _this = this;
        this.router = router;
        this.platform = platform;
        // Set the default settings from the constant
        this.defaultConfig = DEFAULT_CONFIG;
        // If custom config provided with forRoot,
        // use them as default config...
        if (config) {
            this.defaultConfig = config;
        }
        /**
         * Disable Custom Scrollbars if Browser is Mobile
         */
        if (this.platform.ANDROID || this.platform.IOS) {
            this.defaultConfig.customScrollbars = false;
        }
        // Set the config from the default config
        this.config = { this: .defaultConfig };
        // Reload the default settings for the
        // layout on every navigation start
        router.events.subscribe(function (event) {
            if (event instanceof router_1.NavigationStart) {
                _this.setConfig({
                    layout: _this.defaultConfig.layout
                });
            }
        });
        // Create the behavior subject
        this.onConfigChanged = new BehaviorSubject_1.BehaviorSubject(this.config);
    }
    /**
     * Set the new config from given object
     *
     * @param config
     */
    FuseConfigService.prototype.setConfig = function (config) {
        // Set the config from the given object
        // Ugly, but works for now...
        this.config = {
            this: .config,
            config: config,
            layout: {
                this: .config.layout,
                config: .layout
            },
            colorClasses: {
                this: .config.colorClasses,
                config: .colorClasses
            }
        };
        // Trigger the event
        this.onConfigChanged.next(this.config);
    };
    FuseConfigService = __decorate([
        core_1.Injectable(),
        __param(2, core_1.Inject(exports.FUSE_CONFIG)),
        __param(2, core_1.Optional())
    ], FuseConfigService);
    return FuseConfigService;
}());
exports.FuseConfigService = FuseConfigService;
